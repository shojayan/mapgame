//
//  Dialog_QuestionVC.swift
//  MapGame
//
//  Created by omid on 11/29/1397 AP.
//  Copyright © 1397 omid. All rights reserved.
//

import UIKit
import CoreLocation


class Dialog_QuestionVC: UIViewController {

    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var questionText: UILabel!
    @IBOutlet weak var levelName: UILabel!
    @IBOutlet weak var imageQuestion: UIImageView!
    @IBOutlet weak var questionPoint: UILabel!
    @IBOutlet weak var answerBtn: BordredButon!
    
    @IBOutlet weak var height_tbView: NSLayoutConstraint!
    let webserice_getQuestions:GetQuestion = GetQuestion()
    let webserice_answerQuestion:SendAnswer = SendAnswer()
    var questionData:GetQuestion.QuestionModel?
    var selectedAnswerIndex:IndexPath?
    
    
    var barcodeType:GetQuestion.BarCodeType!
    var code:String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        prepareUI()
    }
    func prepareUI(){
        levelName.text = GamePlaySingleton.main.getMarkerInfo()!.name ?? "no name"
        
        
        if GamePlaySingleton.main.getMarkerInfo()!.barcode?.question == nil {
            prepareQuestionAnswers()
        }else{
            self.questionText.text = GamePlaySingleton.main.getMarkerInfo()!.barcode?.question!.body ?? "no question"
            self.height_tbView.constant = 51.0 * CGFloat(GamePlaySingleton.main.getMarkerInfo()?.barcode?.question!.answers?.count ?? 0)
            imageQuestion.sd_setImage(with: convertStringToURL(str: GamePlaySingleton.main.getMarkerInfo()?.barcode?.question!.imageUrl ?? ""),
                                      placeholderImage: #imageLiteral(resourceName: "placeholder") )
        }
        
    }
    func prepareQuestionAnswers(){
        
            loadingViewForThisProject(isShow: true)
            webserice_getQuestions.getQuestion(levelId: GamePlaySingleton.main.getMarkerInfo()?.id ?? 3
                , barcode: code
                , barcodeType: barcodeType
                , getError: { errorStr, errorNumber in
                    DispatchQueue.main.async {
                        self.loadingViewForThisProject(isShow: false)
                    }
                    self.showCustomDialog(message: "خطایی رخ داده است",
                                          btnTitle: "بستن",
                                          completionSHow: nil,
                                          completionDismiss: {
                                            self.dismiss(animated: true, completion: nil)
                    })
            }) { questionDataes in
                
                DispatchQueue.main.async {
                    GamePlaySingleton.main.updateMarkerQuestionData(question: questionDataes)
                    self.loadingViewForThisProject(isShow: false)
                    self.questionData = questionDataes
                    self.tableView.reloadData()
                    self.questionText.text = questionDataes.extra!.body ?? "no question"
                    self.height_tbView.constant = 51.1 * CGFloat(questionDataes.extra!.answers!.count)
                    self.imageQuestion.sd_setImage(with: self.convertStringToURL(str: questionDataes.extra!.imageUrl ?? ""), placeholderImage: #imageLiteral(resourceName: "placeholder"))
                }
                
            }
        
        
    }

    @IBAction func closeDialgAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func routiongAction(_ sender: UIButton) {
        let sheetAlert = UIAlertController(title: "انتخاب نرم افزار", message: "نرم افزار مسیر یابی را انتخاب کنید", preferredStyle: .actionSheet)
        let waze = UIAlertAction(title: "waze", style: .default) { action in
            self.openWaze(location: CLLocationCoordinate2D(latitude: GamePlaySingleton.main.getMarkerInfo()!.barcode!.lat ?? 0 ,
                                                           longitude: GamePlaySingleton.main.getMarkerInfo()!.barcode!.lang ?? 0), notInstalled: {
                                                            self.showCustomDialog(message: "نرم افزار انتخابی نصب نمیباشد",
                                                                                  btnTitle: "بستن",
                                                                                  completionSHow: nil,
                                                                                  completionDismiss: nil)
            })
        }
        
        let googleMap = UIAlertAction(title: "googleMap", style: .default) { action in
            self.openGoogleMap(location: CLLocationCoordinate2D(latitude: GamePlaySingleton.main.getMarkerInfo()!.barcode!.lat ?? 0 ,
                                                                longitude: GamePlaySingleton.main.getMarkerInfo()!.barcode!.lang ?? 0), notInstalled: {
                                                                    self.showCustomDialog(message: "نرم افزار انتخابی نصب نمیباشد",
                                                                                          btnTitle: "بستن",
                                                                                          completionSHow: nil,
                                                                                          completionDismiss: nil)
            })
        }
        let cancel = UIAlertAction(title: "بستن", style: .cancel, handler: nil)
        sheetAlert.addAction(waze)
        sheetAlert.addAction(googleMap)
        sheetAlert.addAction(cancel)
        self.present(sheetAlert, animated: true, completion: nil)
    }
    
    @IBAction func sendAnswerAction(_ sender: BordredButon) {
        
        if questionData != nil,selectedAnswerIndex != nil  {
            print("question --- ",questionData!.extra!.answers![selectedAnswerIndex!.row])
            answerQuestion(questionId: questionData!.extra!.id!,
                           answerId: questionData!.extra!.answers![selectedAnswerIndex!.row].id!)
            
            
        }else if selectedAnswerIndex != nil{
            print("question --- ",GamePlaySingleton.main.getMarkerInfo()!)
            answerQuestion(questionId: GamePlaySingleton.main.getMarkerInfo()!.barcode!.question!.id!,
                           answerId: GamePlaySingleton.main.getMarkerInfo()!.barcode!.question!.answers![selectedAnswerIndex!.row].id!)
            
        }else {
            self.showCustomDialog(message: "لطفا یک پاسخ انتخاب کنید", btnTitle: "بستن", completionSHow: nil , completionDismiss: nil )
        }
        
    }
 
    func answerQuestion(questionId:Int,answerId:Int){
        self.loadingViewForThisProject(isShow: true)
        webserice_answerQuestion.getQuestion(questionId: questionId,
                                             answerId: answerId,
                                             getError: { (errorTxt, errorNumber) in
                                                DispatchQueue.main.async {
                                                    self.loadingViewForThisProject(isShow: false)
                                                }
                                                self.showCustomDialog(message: "خطایی رخ داده است",
                                                                      btnTitle: "دوباره تلاش کنید",
                                                                      completionSHow: nil,
                                                                      completionDismiss: nil )
        }) { answerData in
            if answerData.ok {
                DispatchQueue.main.async {
                    self.loadingViewForThisProject(isShow: false)
                    GamePlaySingleton.main.updateMarketData(data: answerData)
                    GamePlaySingleton.main.changemarker(toImage: #imageLiteral(resourceName: "img_pin_green"), statuse: 1)
                        self.dismiss(animated: true, completion: {
                        let correctVC = self.storyboard?.instantiateViewController(withIdentifier: "DialogCorrectAnswerVC") as! DialogCorrectAnswerVC
                        correctVC.coponCount = answerData.extra!.couponCount!
                        MapVCSingleton.main.mapVC?.present(correctVC, animated: true, completion: nil)
                        })
                }
            }else{
                DispatchQueue.main.async {
                    self.loadingViewForThisProject(isShow: false)
                    GamePlaySingleton.main.changemarker(toImage: #imageLiteral(resourceName: "img_pin_red"), statuse: 2)
                        self.dismiss(animated: true, completion: {
                            let correctVC = self.storyboard?.instantiateViewController(withIdentifier: "DialogWrongAnswerVC") as! DialogWrongAnswerVC
                            MapVCSingleton.main.mapVC?.present(correctVC, animated: true, completion: nil)
                        })
                    
                    
                }
            }
        }
    }
    
}

extension Dialog_QuestionVC:UITableViewDataSource , UITableViewDelegate,AnswerCellDelegate {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if GamePlaySingleton.main.getMarkerInfo()?.barcode?.question != nil {
            return GamePlaySingleton.main.getMarkerInfo()?.barcode?.question!.answers?.count ?? 0
        }else{
            return questionData?.extra?.answers?.count ?? 0
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AnswerCell", for: indexPath) as! AnswerCell
        cell.indexOfCell = indexPath
        cell.delegate = self
        if GamePlaySingleton.main.getMarkerInfo()?.barcode?.question != nil {
            cell.changeButtonTitle(to:GamePlaySingleton.main.getMarkerInfo()?.barcode?.question?.answers?[indexPath.row].body ?? "" )
            return cell
        }else {
            cell.changeButtonTitle(to:self.questionData!.extra!.answers![indexPath.row].body ?? "" )
            return cell
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 51.0 
    }
    func selectedAnswer(indexPath: IndexPath) {
        selectedAnswerIndex = indexPath
    }
}

