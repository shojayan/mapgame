//
//  MainVC.swift
//  MapGame
//
//  Created by omid shojaeian on 12/15/1397 AP.
//  Copyright © 1397 omid. All rights reserved.
//

import UIKit

class MainVC: UIViewController {

    
    @IBOutlet weak var segmentControll: UISegmentedControl!
    
    var pagerVC:PageVC?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareSegmentedControllerAndItemBar()
    }
    

    func prepareSegmentedControllerAndItemBar(){
        let titleTextAttributesNormal = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1),NSAttributedString.Key.font:font_IRANSansMobileFaNum_Bold(size: 15)] as [NSAttributedString.Key : Any]
        let titleTextAttributesSelected = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.2605174184, green: 0.2605243921, blue: 0.260520637, alpha: 1),NSAttributedString.Key.font:font_IRANSansMobileFaNum_Bold(size: 15)] as [NSAttributedString.Key : Any]
        
        segmentControll.setTitleTextAttributes(titleTextAttributesNormal, for: .normal)
        segmentControll.setTitleTextAttributes(titleTextAttributesSelected, for: .selected)
        
        segmentControll.layer.cornerRadius = segmentControll.bounds.height / 2
        segmentControll.layer.masksToBounds = true
        segmentControll.layer.borderWidth = 1
        segmentControll.layer.borderColor = #colorLiteral(red: 0.9963751435, green: 0.8537960649, blue: 0, alpha: 1)
    }
    
    
    @IBAction func segmentAction(_ sender: UISegmentedControl) {
        
        if sender.selectedSegmentIndex == 0 {
            pagerVC!.setViewControllers([pagerVC!.subViewController[0]],
                                        direction: UIPageViewController.NavigationDirection.reverse   , animated: true, completion: nil)
        }else if sender.selectedSegmentIndex == 1 {
            pagerVC!.setViewControllers([pagerVC!.subViewController[1]],
                                        direction: UIPageViewController.NavigationDirection.forward   , animated: false , completion: nil)
        }else if sender.selectedSegmentIndex == 2 {
            pagerVC!.setViewControllers([pagerVC!.subViewController[2]],
                                        direction: UIPageViewController.NavigationDirection.forward   , animated: true, completion: nil)
        }
    }
    
    @IBAction func ExitAppAction(_ sender: UIButton) {
        let askDialog = storyboard?.instantiateViewController(withIdentifier: "Dialog_AskVC") as! Dialog_AskVC
        askDialog.textAlertRecieved = "از بازی خارج می شوید ؟"
        askDialog.actions[.yes] = {
            exit(1)
        }
        askDialog.actions[.no] = {
            askDialog.dismiss(animated: true, completion: nil)
        }
        
        self.present(askDialog, animated: true, completion: nil)
    }
    
    @IBAction func chatShowingAction(_ sender: UIButton) {
        openInstagram(id: "mo.shahr") {
            self.showCustomDialog(message: "برنامه اینستاگرام نصب نمی باشد", btnTitle: "بستن", completionSHow: nil, completionDismiss: nil)
        }
    }
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "mainVCPager" {
            self.pagerVC = (segue.destination as! PageVC)
        }
    }
 

}
