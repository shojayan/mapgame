//
//  PageVC.swift
//  MapGame
//
//  Created by omid shojaeian on 12/15/1397 AP.
//  Copyright © 1397 omid. All rights reserved.
//

import UIKit

class PageVC: UIPageViewController,UIPageViewControllerDelegate,UIPageViewControllerDataSource {

    lazy var subViewController:[UIViewController] = [createViewController(identifie:"ClassificationVC" ),
                                                     createViewController(identifie: "MapVC"),
                                                     createViewController(identifie: "MainOffVC")]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        self.delegate = self
        removeSwipeGesture()
        setViewControllers([subViewController[1]], direction: UIPageViewController.NavigationDirection.forward, animated: true, completion: nil)
    }
    func createViewController(identifie:String)->UIViewController{
        return (storyboard?.instantiateViewController(withIdentifier: identifie))!
    }
    func removeSwipeGesture(){
        for view in self.view.subviews {
            if let subView = view as? UIScrollView {
                subView.isScrollEnabled = false
            }
        }
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
 

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let currentIndex:Int = subViewController.index(of: viewController) ?? 0
        if currentIndex <= 0 {
            return subViewController[1 ]
        }
        return subViewController[currentIndex - 1 ]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let currentIndex:Int = subViewController.index(of: viewController) ?? 0
        if currentIndex >= subViewController.count - 1 {
            return subViewController[0 ]
        }
        return subViewController[currentIndex + 1 ]
    }
    
    
    
}
extension UIPageViewController {
    
    func goToNextPage(animated: Bool = true) {
        guard let currentViewController = self.viewControllers?.first else { return }
        guard let nextViewController = dataSource?.pageViewController(self, viewControllerAfter: currentViewController) else { return }
        setViewControllers([nextViewController], direction: .forward, animated: animated, completion: nil)
    }
    
    func goToPreviousPage(animated: Bool = true) {
        guard let currentViewController = self.viewControllers?.first else { return }
        guard let previousViewController = dataSource?.pageViewController(self, viewControllerBefore: currentViewController) else { return }
        setViewControllers([previousViewController], direction: .reverse, animated: animated, completion: nil)
    }
}
