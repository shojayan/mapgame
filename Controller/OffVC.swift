//
//  OffVC.swift
//  MapGame
//
//  Created by omid shojaeian on 12/15/1397 AP.
//  Copyright © 1397 omid. All rights reserved.
//

import UIKit

class OffVC: UIViewController,UITableViewDataSource,UITableViewDelegate{
   
    var getShopItemData:GetShopes.GetShopes_extraItem!
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var descriptionTxt: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview.backgroundView?.backgroundColor = #colorLiteral(red: 0.1999762356, green: 0.200016588, blue: 0.1999709308, alpha: 1)
        tableview.backgroundColor = #colorLiteral(red: 0.1999762356, green: 0.200016588, blue: 0.1999709308, alpha: 1)
        tableview.tableFooterView = UIView()
        tableview.layer.borderColor = #colorLiteral(red: 1, green: 0.8198096156, blue: 0, alpha: 1)
        tableview.layer.cornerRadius = 30
        tableview.layer.masksToBounds = true
        tableview.layer.borderWidth = 2
        imageView.sd_setImage(with: URL(string: getShopItemData.imageUrl ?? "" ), placeholderImage: #imageLiteral(resourceName: "placeholder") )
        titleLbl.text = getShopItemData.fullName ?? ""
        descriptionTxt.text = getShopItemData.description ?? ""
        //tableview.sizeHeaderToFit()
    }
    
    @IBAction func routiongAction(_ sender: UIButton) {
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getShopItemData.discounts!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PercentCell") as! PercentCell
        if indexPath.row == 0 {
            cell.verticalLineView.isTop = false
        }else if indexPath.row == getShopItemData.discounts!.count - 1 {
            cell.verticalLineView.isDown = false
        }
        cell.percentLbl.text = "\(getShopItemData.discounts![indexPath.row].discountPercent ?? 0)".convertToPersion() + "%"
        cell.offTitle.isHidden = true
        cell.btnCoponNumberCopy.isHidden = true
        cell.descriptionTxt.text = getShopItemData.discounts![indexPath.row].title ?? ""
        
        if getShopItemData.discounts![indexPath.row].uniqueCode != nil,
            getShopItemData.discounts![indexPath.row].uniqueCode! != "" {
            cell.offTitle.isHidden = false
            cell.btnCoponNumberCopy.isHidden = false
            cell.btnCoponNumberCopy.setTitle(getShopItemData.discounts![indexPath.row].uniqueCode, for: .normal)
            cell.btnCoponNumberCopy.setTitle(getShopItemData.discounts![indexPath.row].uniqueCode, for: .selected)
            DispatchQueue.main.async {
                cell.filledPercentLbl(isFill: true)
            }
        }
        
        return cell 
    }
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
}
