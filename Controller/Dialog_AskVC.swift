//
//  Dialog_AskVC.swift
//  MapGame
//
//  Created by omid shojaeian on 12/19/1397 AP.
//  Copyright © 1397 omid. All rights reserved.
//

import UIKit

class Dialog_AskVC: UIViewController {

    @IBOutlet weak var titleIcone: UILabel!
    @IBOutlet weak var textAlert: UILabel!
    @IBOutlet weak var yesBtn: BordredButon!
    @IBOutlet weak var backBtn: BordredButon!
    var textAlertRecieved:String = ""
    typealias actionType = ()->Void
    enum actionsType {
        case yes
        case no
    }
    var actions:[actionsType:actionType] = [actionsType:actionType]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textAlert.text = textAlertRecieved
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    @IBAction func yesAction(_ sender: BordredButon) {
        if actions[.yes] != nil {
            actions[.yes]!()
        }
    }
    @IBAction func backAction(_ sender: BordredButon) {
        if actions[.no] != nil {
            actions[.no]!()
        }
    }
    
}
