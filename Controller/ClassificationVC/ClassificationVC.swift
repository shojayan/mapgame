//
//  classificationVC.swift
//  MapGame
//
//  Created by omid shojaeian on 12/15/1397 AP.
//  Copyright © 1397 omid. All rights reserved.
//

import UIKit

class ClassificationVC: UIViewController {

    
    @IBOutlet weak var segmentedControll: UISegmentedControl!
    
    
    var pageController:ClassificatonPageControllerViewController?
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareSegmentedControllerAndItemBar()
    }
    
    
    func prepareSegmentedControllerAndItemBar(){
        let titleTextAttributesNormal = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1),NSAttributedString.Key.font:font_IRANSansMobileFaNum_Bold(size: 15)] as [NSAttributedString.Key : Any]
        let titleTextAttributesSelected = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.2605174184, green: 0.2605243921, blue: 0.260520637, alpha: 1),NSAttributedString.Key.font:font_IRANSansMobileFaNum_Bold(size: 15)] as [NSAttributedString.Key : Any]
        
        segmentedControll.setTitleTextAttributes(titleTextAttributesNormal, for: .normal)
        segmentedControll.setTitleTextAttributes(titleTextAttributesSelected, for: .selected)
        
        segmentedControll.layer.cornerRadius = segmentedControll.bounds.height / 2
        segmentedControll.layer.masksToBounds = true
        segmentedControll.layer.borderWidth = 1
        segmentedControll.layer.borderColor = #colorLiteral(red: 0.9963751435, green: 0.8537960649, blue: 0, alpha: 1)
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "toPageController" {
            self.pageController = (segue.destination as! ClassificatonPageControllerViewController)
            
        }
    }
 

    @IBAction func segmentAction(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            
         self.pageController?.setViewControllers([pageController!.subViewControllers[0]], direction: UIPageViewController.NavigationDirection.forward  , animated: true, completion: nil)
            
            
        }else if sender.selectedSegmentIndex == 1 {
            
            
         self.pageController?.setViewControllers([pageController!.subViewControllers[1]], direction: UIPageViewController.NavigationDirection.reverse  , animated: true, completion: nil)
            
            
        }
    }
    
    
    
    
}
