//
//  ClassificationInsideVC.swift
//  MapGame
//
//  Created by omid shojaeian on 12/22/1397 AP.
//  Copyright © 1397 omid. All rights reserved.
//

import UIKit

class ClassificationInsideVC: UIViewController,UITableViewDataSource,UITableViewDelegate  {

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var containerEmpty: UIView!
    
    
    let webservice:GetLeaderShip = GetLeaderShip()
    var dataClassification:GetLeaderShip.GetLeaderShipModel?
    var isToday:Bool = false
    
    var refreshControl = UIRefreshControl()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.backgroundView?.backgroundColor = #colorLiteral(red: 0.1999762356, green: 0.200016588, blue: 0.1999709308, alpha: 1)
        tableView.backgroundColor = #colorLiteral(red: 0.1999762356, green: 0.200016588, blue: 0.1999709308, alpha: 1)
        tableView.tableFooterView = UIView()
        
        
        refreshControl.attributedTitle = NSAttributedString(string: "")
        refreshControl.addTarget(self, action: #selector(refresh(_:)), for: UIControl.Event.valueChanged)
        tableView.refreshControl = refreshControl
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getData(showLoader: true)
    }

    @objc func refresh(_ sender:AnyObject) {
        getData(showLoader: false)
    }
    
    
    func getData(showLoader:Bool){
      
        if isToday {
            loadingViewForThisProject(isShow: showLoader)
            let formatter = DateFormatter()
            formatter.timeZone = TimeZone(identifier: "fa_IR")
            //formatter.locale = Locale(identifier: "fa_IR")
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            //formatter.calendar = Calendar(identifier: Calendar.Identifier.persian)
            webservice.getLeaderShipBy(date: formatter.string(from: Date()).addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? "",
                                       getError: { errorStr, errorNum in
                                        DispatchQueue.main.async {
                                            if !showLoader {
                                                self.refreshControl.endRefreshing()
                                            }
                                            self.loadingViewForThisProject(isShow: false)
                                            self.showCustomDialog(message: "خطا رخ داده است", btnTitle: "دوباره تلاش کنید", completionSHow: nil , completionDismiss: {
                                                self.getData(showLoader: showLoader)
                                            })
                                        }
            }) { data in
                DispatchQueue.main.async {
                    if !showLoader {
                        self.refreshControl.endRefreshing()
                    }
                    self.dataClassification = data
                    self.loadingViewForThisProject(isShow: false)
                    self.tableView.reloadData()
                    if data.extra!.count == 0 {
                        self.containerEmpty.isHidden = false
                    }else {
                        self.containerEmpty.isHidden = true
                    }
                }
            }
        }else {
            loadingViewForThisProject(isShow: showLoader)
            webservice.getAllLeaderShip(getError: { errorStr, errorNum in
                                        DispatchQueue.main.async {
                                            if !showLoader {
                                                self.refreshControl.endRefreshing()
                                            }
                                            self.loadingViewForThisProject(isShow: false)
                                            self.showCustomDialog(message: "خطا رخ داده است", btnTitle: "دوباره تلاش کنید", completionSHow: nil , completionDismiss: {
                                                self.getData(showLoader: showLoader)
                                            })
                                        }
            }) { data in
                DispatchQueue.main.async {
                    if !showLoader {
                        self.refreshControl.endRefreshing()
                    }
                    self.dataClassification = data
                    self.loadingViewForThisProject(isShow: false)
                    self.tableView.reloadData()
                    if data.extra!.count == 0 {
                        self.containerEmpty.isHidden = false
                    }else {
                        self.containerEmpty.isHidden = true
                    }
                }
            }
        }
    }
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if dataClassification != nil {
            return dataClassification!.extra!.count
        }else {
            return 0
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ClassificationCell") as! ClassificationCell
        cell.container.layer.borderColor = #colorLiteral(red: 1, green: 0.8196078431, blue: 0, alpha: 1)
        cell.container.layer.borderWidth = 1
        cell.container.layer.cornerRadius = 45
        cell.container.layer.masksToBounds = true
        
        cell.profileImage.layer.cornerRadius = cell.profileImage.frame.height / 2 
        cell.profileImage.layer.masksToBounds = true
        
        
        cell.profileImage.sd_setImage(with: URL(string: dataClassification!.extra![indexPath.row].imageUrl ?? "" ), placeholderImage: #imageLiteral(resourceName: "defaultIcone"))
        cell.titleLbl.text = (dataClassification!.extra![indexPath.row].name ?? "" ) + " " + (dataClassification!.extra![indexPath.row].family ?? "")
        cell.totalPointsLbl.text = "\(dataClassification!.extra![indexPath.row].userScore ?? 0)".convertToPersion()
        
        cell.rowNumber.text = "\(indexPath.row + 1 )".convertToPersion()
        
        return cell 
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}
