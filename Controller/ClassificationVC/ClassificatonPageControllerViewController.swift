//
//  classificatonPageControllerViewController.swift
//  MapGame
//
//  Created by omid shojaeian on 12/22/1397 AP.
//  Copyright © 1397 omid. All rights reserved.
//

import UIKit

class ClassificatonPageControllerViewController: UIPageViewController,UIPageViewControllerDataSource,UIPageViewControllerDelegate {
   

    lazy var subViewControllers:[ClassificationInsideVC] = [createViewController(identifie: "ClassificationInsideVC"),
                                                    createViewController(identifie: "ClassificationInsideVC")]
    override func viewDidLoad() {
        super.viewDidLoad()
        subViewControllers[1].isToday = true
        removeSwipeGesture()
        setViewControllers([subViewControllers[0]], direction: UIPageViewController.NavigationDirection.forward , animated: false, completion: nil)
    }
    
    
    
    
    func createViewController(identifie:String)->ClassificationInsideVC{
        return (storyboard?.instantiateViewController(withIdentifier: identifie)) as! ClassificationInsideVC
    }

    
    
    
    
    
    func removeSwipeGesture(){
        for view in self.view.subviews {
            if let subView = view as? UIScrollView {
                subView.isScrollEnabled = false
            }
        }
    }
    
    
    
    
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let currentIndex:Int = subViewControllers.index(of: viewController as! ClassificationInsideVC) ?? 0
        if currentIndex - 1  < 0 {
            return nil
        }
        return subViewControllers[currentIndex - 1 ]
    }
    
    
    
    
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let currentIndex:Int = subViewControllers.index(of: viewController as! ClassificationInsideVC) ?? 0
        if currentIndex + 1 > subViewControllers.count - 1 {
            return nil
        }
        return subViewControllers[currentIndex + 1 ]
    }

}
