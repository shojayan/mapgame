//
//  RegisterUserVC.swift
//  MapGame
//
//  Created by omid on 12/4/1397 AP.
//  Copyright © 1397 omid. All rights reserved.
//

import UIKit

class RegisterUserVC: UIViewController {

    
    @IBOutlet weak var sendLbl: UILabel!
    
    @IBOutlet weak var sendBtn: BordredButon!
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareKeyBoard()
        prepareKeySendButton()
    }
    
    
    
    func prepareKeySendButton(){
        sendBtn.addTarget(self, action: #selector(touchUpInside(_:)), for: .touchUpInside)
        sendBtn.addTarget(self, action: #selector(touchDown(_:)), for: .touchDown)
        sendBtn.addTarget(self, action: #selector(touchDragOutside(_:)), for: .touchDragOutside)
        sendBtn.addTarget(self, action: #selector(touchDragInside(_:)), for: .touchDragInside)
    }
    func prepareKeyBoard(){
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(hideKeyboard)))
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardAction(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardAction(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    @objc func hideKeyboard(){
        self.view.endEditing(true)
    }
    @objc func keyboardAction(_ sender:Notification){
        let keyboardFrame = (sender.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        if sender.name == UIResponder.keyboardWillShowNotification{
            self.view.transform = CGAffineTransform(translationX: 0, y: -keyboardFrame.height)
        }else if sender.name == UIResponder.keyboardWillHideNotification {
            self.view.transform = CGAffineTransform(translationX: 0, y: 0)
        }
    }
    @objc func touchUpInside(_ sender:BordredButon){
        
        UIView.animate(withDuration: 0.3) {
            self.sendLbl.alpha = 1
            sender.layer.borderColor? = UIColor(red: 1, green: 0.8233510256, blue: 0, alpha: 1).cgColor
        }
    }
    @objc func touchDown(_ sender:BordredButon){
        self.sendLbl.alpha = 0.3
        sender.layer.borderColor? = UIColor(red: 1, green: 0.8233510256, blue: 0, alpha: 0.3).cgColor
    }
    
    
    @objc func touchDragInside(_ sender:BordredButon) {
        UIView.animate(withDuration: 0.3) {
            self.sendLbl.alpha = 0.3
            sender.layer.borderColor? = UIColor(red: 1, green: 0.8233510256, blue: 0, alpha: 0.3).cgColor
        }
    }
    @objc func touchDragOutside(_ sender:BordredButon){
        UIView.animate(withDuration: 0.3) {
            self.sendLbl.alpha = 1
            sender.layer.borderColor? = UIColor(red: 1, green: 0.8233510256, blue: 0, alpha: 1).cgColor
        }
    }
    @IBAction func closeAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func sendAction(_ sender: BordredButon) {
    }
    
}
