//
//  RegisterUserVC.swift
//  MapGame
//
//  Created by omid on 12/1/1397 AP.
//  Copyright © 1397 omid. All rights reserved.
//

import UIKit

class GetUserNumberVC: UIViewController {

    
    @IBOutlet var StepsLbl: [UILabel]!
    @IBOutlet weak var sendButton: BordredButon!
    @IBOutlet weak var sendLbl:UILabel!
    @IBOutlet weak var mobileNumberField: UITextField!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareKeyBoard()
        StepsLbl.forEach { lable in
            lable.layer.borderColor = #colorLiteral(red: 1, green: 0.8233510256, blue: 0, alpha: 1)
            lable.layer.borderWidth = 1
        }
        sendButton.addTarget(self, action: #selector(touchUpInside(_:)), for: .touchUpInside)
        sendButton.addTarget(self, action: #selector(touchDown(_:)), for: .touchDown)
        sendButton.addTarget(self, action: #selector(touchDragOutside(_:)), for: .touchDragOutside)
        sendButton.addTarget(self, action: #selector(touchDragInside(_:)), for: .touchDragInside)
    }
    
    
    
    
    func prepareKeyBoard(){
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(hideKeyboard)))
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardAction(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardAction(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func hideKeyboard(){
        self.view.endEditing(true)
    }
    @objc func keyboardAction(_ sender:Notification){
        let keyboardFrame = (sender.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        if sender.name == UIResponder.keyboardWillShowNotification{
            self.view.transform = CGAffineTransform(translationX: 0, y: -keyboardFrame.height)
        }else if sender.name == UIResponder.keyboardWillHideNotification {
            self.view.transform = CGAffineTransform(translationX: 0, y: 0)
        }
    }
    
    
    @objc func touchUpInside(_ sender:BordredButon){
        
        UIView.animate(withDuration: 0.3) {
            self.sendLbl.alpha = 1
            sender.layer.borderColor? = UIColor(red: 1, green: 0.8233510256, blue: 0, alpha: 1).cgColor
        }
    }
    @objc func touchDown(_ sender:BordredButon){
        self.sendLbl.alpha = 0.3
        sender.layer.borderColor? = UIColor(red: 1, green: 0.8233510256, blue: 0, alpha: 0.3).cgColor
    }
    
    
    @objc func touchDragInside(_ sender:BordredButon) {
        UIView.animate(withDuration: 0.3) {
            self.sendLbl.alpha = 0.3
            sender.layer.borderColor? = UIColor(red: 1, green: 0.8233510256, blue: 0, alpha: 0.3).cgColor
        }
    }
    @objc func touchDragOutside(_ sender:BordredButon){
        UIView.animate(withDuration: 0.3) {
            self.sendLbl.alpha = 1
            sender.layer.borderColor? = UIColor(red: 1, green: 0.8233510256, blue: 0, alpha: 1).cgColor
        }
    }
    
    
    @IBAction func sendAction(_ sender: BordredButon) {
        
        if Reachability.isConnectedToNetwork() {
            self.performSegue(withIdentifier: "tocodeVerify", sender: self)
        }else {
            self.createCancellableGeneralAlert(title: "توجه", message: "مشکل در اینترنت", cancelTitle: "بستن")
        }
        
    }
       @IBAction func register_logIn_Action(_ sender: UIButton) {
    }
    
    @IBAction func backAction(_ sender: UIButton) {
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
