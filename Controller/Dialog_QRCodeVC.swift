//
//  Dialog_QRCodeVC.swift
//  MapGame
//
//  Created by omid on 11/29/1397 AP.
//  Copyright © 1397 omid. All rights reserved.
//

import UIKit
import CoreLocation
import WazeKit


class Dialog_QRCodeVC: UIViewController {

    
    
    @IBOutlet weak var nameLevel: UILabel!
    @IBOutlet weak var pointLevelLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var helpLbl: UILabel!
    @IBOutlet weak var barcodeLbl_onButton: UILabel!
    @IBOutlet weak var panelLbl_onButton: UILabel!
    @IBOutlet weak var scanBarcodBtn: BordredButon!
    @IBOutlet weak var routingBtn: BordredButon!
    @IBOutlet weak var otherWayBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareButtons()
        prepareUI()
    }
    
    func prepareUI(){
        if GamePlaySingleton.main.getMarkerInfo()!.barcode != nil { //, GamePlaySingleton.main.getMarkerInfo()!.barcode?.question == nil {
            nameLevel.text = GamePlaySingleton.main.getMarkerInfo()!.name ?? ""
            pointLevelLbl.text = "امتیاز معما : \((GamePlaySingleton.main.getMarkerInfo()!.barcodePoint ?? 0 ) + (GamePlaySingleton.main.getMarkerInfo()!.questionPoint ?? 0 ))"
            addressLbl.text = GamePlaySingleton.main.getMarkerInfo()!.barcode!.address ?? ""
            helpLbl.text = GamePlaySingleton.main.getMarkerInfo()!.barcode!.help ?? ""
            if GamePlaySingleton.main.getMarkerInfo()!.barcode!.uniqueCodeHelp != nil , GamePlaySingleton.main.getMarkerInfo()!.barcode!.uniqueCodeHelp!.count > 0 {
                self.otherWayBtn.isHidden = false
            }else {
               self.otherWayBtn.isHidden = true
            }
        }
    }
    func prepareButtons(){
        scanBarcodBtn.addTarget(self, action: #selector(touchUpInside(_:)), for: .touchUpInside)
        scanBarcodBtn.addTarget(self, action: #selector(touchDown(_:)), for: .touchDown)
        scanBarcodBtn.addTarget(self, action: #selector(touchDragOutside(_:)), for: .touchDragOutside)
        scanBarcodBtn.addTarget(self, action: #selector(touchDragInside(_:)), for: .touchDragInside)
        
        routingBtn.addTarget(self, action: #selector(touchUpInside(_:)), for: .touchUpInside)
        routingBtn.addTarget(self, action: #selector(touchDown(_:)), for: .touchDown)
        routingBtn.addTarget(self, action: #selector(touchDragOutside(_:)), for: .touchDragOutside)
        routingBtn.addTarget(self, action: #selector(touchDragInside(_:)), for: .touchDragInside)
    }
    
    @objc func touchUpInside(_ sender:BordredButon){
        if sender == scanBarcodBtn {
            UIView.animate(withDuration: 0.3) {
                self.barcodeLbl_onButton.alpha = 1
                sender.layer.borderColor? = UIColor(red: 1, green: 0.8233510256, blue: 0, alpha: 1).cgColor
            }
        }else if sender == routingBtn {
            UIView.animate(withDuration: 0.3) {
                self.panelLbl_onButton.alpha = 1
                sender.layer.borderColor? = UIColor(red: 1, green: 0.8233510256, blue: 0, alpha: 1).cgColor
            }
        }
        
    }
    @objc func touchDown(_ sender:BordredButon){
        if sender == scanBarcodBtn {
            self.barcodeLbl_onButton.alpha = 0.3
            sender.layer.borderColor? = UIColor(red: 1, green: 0.8233510256, blue: 0, alpha: 0.3).cgColor
        }else if sender == routingBtn {
                   self.panelLbl_onButton.alpha = 0.3
            sender.layer.borderColor? = UIColor(red: 1, green: 0.8233510256, blue: 0, alpha: 0.3).cgColor
        }
    }
    
    
    @objc func touchDragInside(_ sender:BordredButon) {
        if sender == scanBarcodBtn {
            UIView.animate(withDuration: 0.3) {
             self.barcodeLbl_onButton.alpha = 0.3
                sender.layer.borderColor? = UIColor(red: 1, green: 0.8233510256, blue: 0, alpha: 0.3).cgColor
            }
        }else if sender == routingBtn {
            UIView.animate(withDuration: 0.3) {
                self.panelLbl_onButton.alpha = 0.3
                sender.layer.borderColor? = UIColor(red: 1, green: 0.8233510256, blue: 0, alpha: 0.3).cgColor
            }
        }
    }
    @objc func touchDragOutside(_ sender:BordredButon){
        if sender == scanBarcodBtn {
            UIView.animate(withDuration: 0.3) {
                self.barcodeLbl_onButton.alpha = 1
                sender.layer.borderColor? = UIColor(red: 1, green: 0.8233510256, blue: 0, alpha: 1).cgColor
            }
        }else if sender == routingBtn {
            UIView.animate(withDuration: 0.3) {
                self.panelLbl_onButton.alpha = 1
                sender.layer.borderColor? = UIColor(red: 1, green: 0.8233510256, blue: 0, alpha: 1).cgColor
            }
        }
    }
    
    
    @IBAction func closeDialogAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func scanBarcodeAction(_ sender: BordredButon) {
        if let _ = self.storyboard?.instantiateViewController(withIdentifier: "QRCodeReadeVC"),MapVCSingleton.main.mapVC != nil {
            self.dismiss(animated: true, completion: {
                
                
                    MapVCSingleton.main.mapVC!.createGap(withDuration: 1.0, startGap: { mapVC in
                        
                        
                        mapVC.loadingViewForThisProject(isShow: true)
                    }, endGap: { mapVC in
                        
                        
                        mapVC.loadingViewForThisProject(isShow: false)
                        let qrCodeReaderVC = (self.storyboard?.instantiateViewController(withIdentifier: "QRCodeReadeVC")) as! QRCodeReadeVC
                        mapVC.present(qrCodeReaderVC , animated: true, completion: nil)
                    })
                
            })
        }else {
            print("error with incomeVC from Dialog_QRCodeVC")
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func routingAction(_ sender: BordredButon) {
        let sheetAlert = UIAlertController(title: "انتخاب نرم افزار", message: "نرم افزار مسیر یابی را انتخاب کنید", preferredStyle: .actionSheet)
        let waze = UIAlertAction(title: "waze", style: .default) { action in
            self.openWaze(location: CLLocationCoordinate2D(latitude: GamePlaySingleton.main.getMarkerInfo()!.barcode!.lat ?? 0 ,
                                                           longitude: GamePlaySingleton.main.getMarkerInfo()!.barcode!.lang ?? 0), notInstalled: {
                                                            self.showCustomDialog(message: "نرم افزار انتخابی نصب نمیباشد",
                                                                                  btnTitle: "بستن",
                                                                                  completionSHow: nil,
                                                                                  completionDismiss: nil)
            })
        }
        
        let googleMap = UIAlertAction(title: "googleMap", style: .default) { action in
            self.openGoogleMap(location: CLLocationCoordinate2D(latitude: GamePlaySingleton.main.getMarkerInfo()!.barcode!.lat ?? 0 ,
                                                           longitude: GamePlaySingleton.main.getMarkerInfo()!.barcode!.lang ?? 0), notInstalled: {
                                                            self.showCustomDialog(message: "نرم افزار انتخابی نصب نمیباشد",
                                                                                  btnTitle: "بستن",
                                                                                  completionSHow: nil,
                                                                                  completionDismiss: nil)
            })
        }
        let cancel = UIAlertAction(title: "بستن", style: .cancel, handler: nil)
        sheetAlert.addAction(waze)
        sheetAlert.addAction(googleMap)
        sheetAlert.addAction(cancel)
        self.present(sheetAlert, animated: true, completion: nil)
    }
    @IBAction func otherAction(_ sender: UIButton) {
        //GamePlaySingleton.main.getMarkerInfo()!.barcode!.uniqueCodeHelp!
        let instagramDialog = storyboard?.instantiateViewController(withIdentifier: "Dialog_instagramSearch") as! Dialog_instagramSearch
        instagramDialog.instagramText = GamePlaySingleton.main.getMarkerInfo()!.barcode!.uniqueCodeHelp
        instagramDialog.levelName = GamePlaySingleton.main.getMarkerInfo()!.name ?? ""
        dismiss(animated: true) {
            MapVCSingleton.main.mapVC?.present(instagramDialog, animated: true, completion: nil)
        }
    }
    
}
