//
//  RegisterUser.swift
//  MapGame
//
//  Created by omid shojaeian on 12/13/1397 AP.
//  Copyright © 1397 omid. All rights reserved.
//

import UIKit
import FirebaseMessaging

class RegisteringUserVC: UIViewController,UINavigationControllerDelegate, UIImagePickerControllerDelegate{

    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var addImageBtn: UIButton!
    @IBOutlet weak var nameTxt: UITextField!
    @IBOutlet weak var lastNameTxt: UITextField!
    @IBOutlet weak var registerBtnLbl: UILabel!
    @IBOutlet weak var registerBtn: BordredButon!
    
    let webservice_RegisterUser:RegisterFcmToken = RegisterFcmToken()
    var isFromMapVc:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareUI()
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "exitToSplash" {
            (segue.destination as! SplashVC).isRegisterCompleted = true
        }
    }
    
    
    func prepareUI(){
        if self.readRegisteredUserInfo() != nil {
            profileImage.image = self.readUserImage()
            nameTxt.text = self.readRegisteredUserInfo()!.extra!.name ?? ""
            lastNameTxt.text = self.readRegisteredUserInfo()!.extra!.family ?? ""
        }
        profileImage.layer.borderWidth = 1
        profileImage.layer.cornerRadius = profileImage.frame.height / 2
        profileImage.layer.borderColor = #colorLiteral(red: 1, green: 0.8233510256, blue: 0, alpha: 1)
        profileImage.layer.masksToBounds = true
        
        addImageBtn.layer.borderWidth = 1
        addImageBtn.layer.cornerRadius = addImageBtn.frame.height / 2
        addImageBtn.layer.borderColor = #colorLiteral(red: 1, green: 0.8233510256, blue: 0, alpha: 1)
        addImageBtn.layer.masksToBounds = true
        
        
        registerBtn.addTarget(self, action: #selector(touchUpInside(_:)), for: .touchUpInside)
        registerBtn.addTarget(self, action: #selector(touchDown(_:)), for: .touchDown)
        registerBtn.addTarget(self, action: #selector(touchDragOutside(_:)), for: .touchDragOutside)
        registerBtn.addTarget(self, action: #selector(touchDragInside(_:)), for: .touchDragInside)
        
        nameTxt.layer.cornerRadius = nameTxt.frame.height / 2
        nameTxt.layer.masksToBounds = true
        lastNameTxt.layer.cornerRadius = lastNameTxt.frame.height / 2
        lastNameTxt.layer.masksToBounds = true
        prepareKeyBoard()
        
        
    }
    
    
    @objc func touchUpInside(_ sender:BordredButon){
        
            UIView.animate(withDuration: 0.3) {
                self.registerBtnLbl.alpha = 1
            }
    }
    
    @objc func touchDown(_ sender:BordredButon){
            self.registerBtnLbl.alpha = 0.3
    }
    
    
    @objc func touchDragInside(_ sender:BordredButon) {
       
            UIView.animate(withDuration: 0.3) {
                self.registerBtnLbl.alpha = 0.3
            }
    
    }
    @objc func touchDragOutside(_ sender:BordredButon){
            UIView.animate(withDuration: 0.3) {
                self.registerBtnLbl.alpha = 1
            }
    }
    func prepareKeyBoard(){
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(hideKeyboard)))
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardAction(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardAction(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    @objc func hideKeyboard(){
        self.view.endEditing(true)
    }
    
    
    
    @objc func keyboardAction(_ sender:Notification){
        let keyboardFrame = (sender.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        if sender.name == UIResponder.keyboardWillShowNotification{
            self.view.transform = CGAffineTransform(translationX: 0, y: -keyboardFrame.height)
        }else if sender.name == UIResponder.keyboardWillHideNotification {
            self.view.transform = CGAffineTransform(translationX: 0, y: 0)
        }
    }
    
   
    @IBAction func addImageAction(_ sender: UIButton) {
        imagePicker()
    }
    @IBAction func closeAction(_ sender: UIButton) {
        
            self.registerUserHere(image: profileImage.image ?? #imageLiteral(resourceName: "defaultIcone"), complection: {
                if self.isFromMapVc {
                    self.dismiss(animated: true, completion: {
                    MapVCSingleton.main.mapVC!.prepareUserInformations()
                    })
                }else {
                    self.performSegue(withIdentifier: "exitToSplash", sender: self)
                }
            })
        
        
    }
    
    @IBAction func registerUserInfoAction(_ sender: UIButton) {
        
            self.registerUserHere(image: profileImage.image ?? #imageLiteral(resourceName: "defaultIcone"), complection: {
                
                if self.isFromMapVc {
                    self.dismiss(animated: true, completion: nil)
                }else {
                    self.performSegue(withIdentifier: "exitToSplash", sender: self)
                }
            })
        
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)
        guard let image = info[.editedImage] as? UIImage else {
            print("No image found")
            return
        }
        profileImage.image = image
    }
    
    
    func imagePicker(){
        let vc = UIImagePickerController()
        vc.allowsEditing = true
        vc.delegate = self
        
        
        let alertSheet = UIAlertController(title: "منبع", message: "منبع خود را انتخاب کنید", preferredStyle: .actionSheet)
        let libraryAction = UIAlertAction(title: "گالری", style: .default) { action in
            vc.sourceType = .photoLibrary
            self.present(vc, animated: true)
        }
        let cameraAction = UIAlertAction(title: "دوربین", style: .default) { action in
            vc.sourceType = .camera
            vc.showsCameraControls = true
            vc.cameraCaptureMode = .photo
            self.present(vc, animated: true)
        }
        alertSheet.addAction(libraryAction)
        alertSheet.addAction(cameraAction)
        
        present(alertSheet, animated: true)
    }
    func registerUserHere(image:UIImage,complection:@escaping ()->Void){
        guard Messaging.messaging().fcmToken != nil  else {
            self.showCustomDialog(message: "خطایی رخ داده است" + " fcm",
                btnTitle: "تلاش مجدد",
                completionSHow: nil,
                completionDismiss: nil )
            return
        }
        
        let fcmParts = Messaging.messaging().fcmToken!.components(withLength: 4)
        var name:String = ""
        var family:String = ""
        
        if nameTxt.text != nil , nameTxt.text!.count != 0 {
            name = nameTxt.text!
        }else{
            name = (fcmParts.count > 2 ? fcmParts[0]:"nofcm")
        }
        
        
        if lastNameTxt.text != nil , lastNameTxt.text!.count != 0 {
            family = lastNameTxt.text!
        }else {
            family = (fcmParts.count > 2 ? fcmParts[1]:"nofcm2")
        }
        self.loadingViewForThisProject(isShow: true)
        webservice_RegisterUser.registerUser(name: name,
                                             familyName: family,
                                             imageBase64: self.imageWithImage(image: image,
                                                                              scaledToSize: CGSize(width: 256, height: 256)).toBase64() ?? "noBase64",
                                             getError: { errorStr, errorInt in
                                                self.showCustomDialog(message: "خطایی رخ داده است",
                                                    btnTitle: "تلاش مجدد",
                                                    completionSHow: nil,
                                                    completionDismiss: {DispatchQueue.main.async {
                                                        self.loadingViewForThisProject(isShow: false )
                                                        }})
                                                
        }) { dataes in
            DispatchQueue.main.async {
                self.loadingViewForThisProject(isShow: false)
                self.registerUser(info: dataes, userImage: self.imageWithImage(image: image,
                                                                               scaledToSize: CGSize(width: 256, height: 256)))
                print(dataes)
                complection()
            }
        }
    }
    
}
