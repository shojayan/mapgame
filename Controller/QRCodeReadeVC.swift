//
//  QRCodeReadeVC.swift
//  MapGame
//
//  Created by omid on 11/28/1397 AP.
//  Copyright © 1397 omid. All rights reserved.
//

import UIKit
import AVKit


class QRCodeReadeVC: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    
    @IBOutlet weak var captureView: UIView!
    @IBOutlet weak var maskview: MaskView!
    @IBOutlet weak var scanIndicator: IndicatorQR_Reader!
    @IBOutlet weak var levelName: UILabel!
    @IBOutlet weak var barcodePointLbl: UILabel!
    @IBOutlet weak var textToSearch: UITextField!
    
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareKeyBoard()
        levelName.text = GamePlaySingleton.main.getMarkerInfo()!.name ?? "no name"
        barcodePointLbl.text = "امتیاز اسکن بارکد " + "\(GamePlaySingleton.main.getMarkerInfo()!.barcodePoint ?? 0 )"
        captureSession = AVCaptureSession()
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
        let videoInput: AVCaptureDeviceInput
        
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }
        
        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            failed()
            return
        }
        
        let metadataOutput = AVCaptureMetadataOutput()
        
        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)
            
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.qr, .aztec,.code128, .code39, .code39Mod43, .code93, .dataMatrix, .ean13,.ean8]
        } else {
            failed()
            return
        }
        
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = captureView.layer.bounds
        previewLayer.videoGravity = .resizeAspectFill
        captureView.layer.addSublayer(previewLayer)
        
        captureSession.startRunning()
    }
    
    @IBAction func scanCodeAction(_ sender: UIButton) {
        
        
        scanIndicator.startAnimating()
        captureSession.startRunning()
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func searchAction(_ sender: UIButton) {
        print(textToSearch.text ?? "---")
        found(code: textToSearch.text ?? "a")
    }
    
    func prepareKeyBoard(){
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(hideKeyboard)))
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardAction(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardAction(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    @objc func hideKeyboard(){
        self.view.endEditing(true)
    }
    @objc func keyboardAction(_ sender:Notification){
        let keyboardFrame = (sender.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        if sender.name == UIResponder.keyboardWillShowNotification{
            self.view.transform = CGAffineTransform(translationX: 0, y: -keyboardFrame.height)
        }else if sender.name == UIResponder.keyboardWillHideNotification {
            self.view.transform = CGAffineTransform(translationX: 0, y: 0)
        }
    }
    func failed() {
        let ac = UIAlertController(title: "Scanning not supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera.", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
        captureSession = nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if (captureSession?.isRunning == false) {
            captureSession.startRunning()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if (captureSession?.isRunning == true) {
            captureSession.stopRunning()
        }
    }
    
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        captureSession.stopRunning()
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            found(code: stringValue)
            
        }
    }
    
    
    
    func found(code: String) {
        print(code)
        scanIndicator.stopAnimating()
         let getQuestion = GetQuestion()
        
        if GamePlaySingleton.main.getMarkerInfo()!.id != nil  {
            
            
            self.loadingViewForThisProject(isShow: true)
            getQuestion.getQuestion(levelId: GamePlaySingleton.main.getMarkerInfo()!.id!,
                                    barcode: code,
                                    barcodeType: .uniqCode,
                                    getError: { errorStr, errorNum in
                                        self.showCustomDialog(message: "خطایی رخداده" + "\(errorNum)",
                                                              btnTitle: "تلاش مجدد",
                                                              completionSHow: nil,
                                                              completionDismiss: {DispatchQueue.main.async {
                                                                self.loadingViewForThisProject(isShow: false)
                                                                }})
            }) { questionData in
                if questionData.ok {
                    DispatchQueue.main.async {
                        self.loadingViewForThisProject(isShow: false)
                        if let questionVC = self.storyboard?.instantiateViewController(withIdentifier: "Dialog_QuestionVC"){
                            (questionVC as! Dialog_QuestionVC).barcodeType = .uniqCode
                            (questionVC as! Dialog_QuestionVC).code = code
                            if MapVCSingleton.main.mapVC != nil {
                                self.dismiss(animated: true) {
                                    MapVCSingleton.main.mapVC!.createGap(withDuration: 1.0,
                                                                         startGap: { mapVC  in
                                                                         mapVC.loadingViewForThisProject(isShow: true)
                                    }) { mapVC in
                                        mapVC.loadingViewForThisProject(isShow: false)
                                        mapVC.present(questionVC, animated: true, completion: nil)
                                    }
                                }
                            }else {
                                self.showCustomDialog(message: "مشکل در نرم افزار - نرم افزار را بسته و دوباره اجرا کنید", btnTitle: "بستن", completionSHow: nil , completionDismiss: nil )
                            }
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.loadingViewForThisProject(isShow: false)
                        self.showCustomDialog(message: "بار کد اشتباه می باشد",
                                              btnTitle: "تلاش مجدد",
                                              completionSHow: nil,
                                              completionDismiss: {
                                                self.scanIndicator.startAnimating()
                                                self.captureSession.startRunning()
                                                })
                    }
                }
            }
        }else{
            self.showCustomDialog(message: "مشکل در نرم افزار - نرم افزار را بسته و دوباره اجرا کنید", btnTitle: "بستن", completionSHow: nil , completionDismiss: nil )
        }
       
        
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
}



