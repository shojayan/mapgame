//
//  WrongAnswerVC.swift
//  MapGame
//
//  Created by omid shojaeian on 12/12/1397 AP.
//  Copyright © 1397 omid. All rights reserved.
//

import UIKit

class DialogWrongAnswerVC: UIViewController {

    
    @IBOutlet weak var barcodePoint: UILabel!
    @IBOutlet weak var levelPoint: UILabel!
    @IBOutlet weak var totalPoint: UILabel!
    let pointText:String = "امتیاز"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        prepareUI()
    }
    
    func prepareUI(){
         if GamePlaySingleton.main.getMarkerInfo() != nil {
         barcodePoint.text = "\(GamePlaySingleton.main.getMarkerInfo()!.barcodePoint ?? 0)" + pointText
            
            
        totalPoint.text = "\((GamePlaySingleton.main.getMarkerInfo()!.bouncePoint ?? 0) + (GamePlaySingleton.main.getMarkerInfo()!.barcodePoint ?? 0))" + pointText
        }
    }

    @IBAction func clodeDialog(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
