//
//  ViewController.swift
//  MapGame
//
//  Created by omid on 11/23/1397 AP.
//  Copyright © 1397 omid. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import GooglePlaces
import SDWebImage

class MapVC: UIViewController {
    @IBAction func goToSideMenu(segue: UIStoryboardSegue) {}
    @IBOutlet weak var mapView: GMSMapView!
    
    
    @IBOutlet weak var imageUser: UIImageView!
    @IBOutlet weak var nameUser: UILabel!
    @IBOutlet weak var ratePoint: UILabel!
    @IBOutlet weak var pointNumber: UILabel!
    @IBOutlet weak var mapGuideBtn: UIButton!
    @IBOutlet weak var nearMapBtn: UIButton!
    
    
    
    var loadigView: UIView!
    
    let webservice_gamePlay:GamePlay = GamePlay()
    
    
    var myLocation:CLLocation!
    let locationmanager:CLLocationManager = CLLocationManager()
    var allMarkers:[GMSMarker] = [GMSMarker]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        MapVCSingleton.main.mapVC = self
        prepareLocationManager()
        prepareUserInformations()
        mapView.delegate = self
        //mapView.settings.myLocationButton = true
        mapView.isMyLocationEnabled = true
        // 👇important Function 👇
        self.prepareLevelsMarker()
        // ☝️important Function ☝️
        do {
            // Set the map style by passing the URL of the local file. Make sure style.json is present in your project
            if let styleURL = Bundle.main.url(forResource: "map_style", withExtension: "json") {
                mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                print("Unable to find style.json")
            }
        } catch {
            print("The style definition could not be loaded: \(error)")
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
            let camera = GMSCameraPosition.camera(withTarget: CLLocationCoordinate2D(latitude: 36.686834, longitude: 48.495697), zoom: 16.0)
        DispatchQueue.main.async {
            self.mapView.animate(to: camera)
        }
            prepareUserInformations()
    }
    
    
    func prepareLevelsMarker(){
        guard GamePlaySingleton.main.levelesModel != nil else {
            return
        }
        
        for (index,levelItem) in GamePlaySingleton.main.levelesModel!.extra!.enumerated() {
            let marker = GMSMarker(position: CLLocationCoordinate2D(latitude: CLLocationDegrees(levelItem.barcode!.lat!) ,
                                                                    longitude: CLLocationDegrees(levelItem.barcode!.lang!)))
            let width:CGFloat = 50
            let height:CGFloat = 50
            //mapView.clear()
            if levelItem.barcode!.question == nil {
                marker.icon = self.imageWithImage(image: #imageLiteral(resourceName: "img_pin_yellow"), scaledToSize: CGSize(width: width, height: height))
            }else if levelItem.barcode!.question!.answerStatus! == 0  {
                marker.icon = self.imageWithImage(image: #imageLiteral(resourceName: "img_pin_blue"), scaledToSize: CGSize(width: width, height: height))
            }else if levelItem.barcode!.question!.answerStatus! == 1  {
                marker.icon = self.imageWithImage(image: #imageLiteral(resourceName: "img_pin_green"), scaledToSize: CGSize(width: width, height: height))
            }else if levelItem.barcode!.question!.answerStatus! == 2  {
                marker.icon = self.imageWithImage(image: #imageLiteral(resourceName: "img_pin_red"), scaledToSize: CGSize(width: width, height: height))
            }
            marker.userData = index
            marker.map = self.mapView
            allMarkers.append(marker)
        }
    }
    func prepareLocationManager(){
        locationmanager.delegate = self
        locationmanager.requestAlwaysAuthorization()
        locationmanager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        locationmanager.startUpdatingLocation()
    }
    func prepareUserInformations(){
        mapGuideBtn.layer.cornerRadius = ratePoint.frame.height / 2
        mapGuideBtn.layer.masksToBounds = true
        mapGuideBtn.layer.borderColor = #colorLiteral(red: 1, green: 0.8198096156, blue: 0, alpha: 1)
        mapGuideBtn.layer.borderWidth = 2
        nearMapBtn.layer.cornerRadius = pointNumber.frame.height / 2
        nearMapBtn.layer.masksToBounds = true
        nearMapBtn.layer.borderColor = #colorLiteral(red: 1, green: 0.8198096156, blue: 0, alpha: 1)
        nearMapBtn.layer.borderWidth = 2
        
        imageUser.layer.cornerRadius = imageUser.frame.height / 2
        imageUser.layer.masksToBounds = true
        imageUser.layer.borderWidth = 2
        imageUser.layer.borderColor = #colorLiteral(red: 1, green: 0.8198096156, blue: 0, alpha: 1)
        
        if self.readRegisteredUserInfo() != nil {
            imageUser.sd_setImage(with: URL(string: self.readRegisteredUserInfo()!.extra!.imageUrl ?? "" ), placeholderImage: #imageLiteral(resourceName: "defaultIcone") )
            
            if (self.readRegisteredUserInfo()!.extra!.userRate ?? 0 ) > 0 {
             ratePoint.text = "\(self.readRegisteredUserInfo()!.extra!.userRate!)"
            }
            if (self.readRegisteredUserInfo()!.extra!.userScore ?? 0 ) > 0 {
                pointNumber.text = "\(self.readRegisteredUserInfo()!.extra!.userScore!)"
            }
            nameUser.text = (self.readRegisteredUserInfo()!.extra!.name ?? "") + " "  + (self.readRegisteredUserInfo()!.extra!.family ?? "")
        }else {
            imageUser.image = #imageLiteral(resourceName: "defaultIcone")
            nameUser.text = "بدون نام"
        }
    }
    
    
    @IBAction func mapGuiderSowAction(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "GuideVC") as! GuideVC
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func nearestMapAction(_ sender: UIButton) {
    }
    @IBAction func editeUserAction(_ sender: UIButton) {
        let editVC = storyboard?.instantiateViewController(withIdentifier: "RegisteringUserVC") as! RegisteringUserVC
        editVC.isFromMapVc = true
        present(editVC, animated: true, completion: nil)
    }
    
    
    var firstUpdateLocation = true
}

extension MapVC:CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        myLocation = manager.location
        if firstUpdateLocation {
            let camera = GMSCameraPosition.camera(withTarget: CLLocationCoordinate2D(latitude: 36.686834, longitude: 48.495697), zoom: 16.0)
            self.mapView.animate(to: camera)
            firstUpdateLocation = false 
        }
        
    }
    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
    }
}


extension MapVC: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
       
            //incomVC
            GamePlaySingleton.main.markerDataIndexFromMapVCUpdate(index: (marker.userData as! Int))
        
            print(GamePlaySingleton.main.getMarkerInfo()!)
        
            if GamePlaySingleton.main.getMarkerInfo()?.barcode?.question != nil {
                if GamePlaySingleton.main.getMarkerInfo()?.barcode?.question!.answerStatus! == 0 {
                    ///// question dialog
                    if let questionVC = (storyboard?.instantiateViewController(withIdentifier: "Dialog_QuestionVC") as? Dialog_QuestionVC) {
                        self.present(questionVC, animated: true, completion: nil)
                    }
                }else if GamePlaySingleton.main.getMarkerInfo()?.barcode?.question!.answerStatus! == 1 {
                        ///// Correct Answer dialog
                        if let correctVC = (storyboard?.instantiateViewController(withIdentifier: "DialogCorrectAnswerVC") as? DialogCorrectAnswerVC) {
                            self.present(correctVC, animated: true, completion: nil)
                    }
                }else if GamePlaySingleton.main.getMarkerInfo()?.barcode?.question!.answerStatus! == 2 {
                    ///// wrong Answer dialog
                    if let wrongVC = (storyboard?.instantiateViewController(withIdentifier: "DialogWrongAnswerVC") as? DialogWrongAnswerVC) {
                        self.present(wrongVC, animated: true, completion: nil)
                    }
                }
            }else{
                ///// dialogQrcode
                 if let dialogQrcode = (storyboard?.instantiateViewController(withIdentifier: "Dialog_QRCodeVC") as? Dialog_QRCodeVC) {
                    self.present(dialogQrcode, animated: true, completion: nil)
                }
            }
            
        
        return true
    }
    
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        let tilt = mapView.camera.zoom < 15 ? 0:90
        let camera = GMSCameraPosition.camera(withTarget: mapView.camera.target,
                                              zoom: mapView.camera.zoom,
                                              bearing: mapView.camera.bearing,
                                              viewingAngle: Double(tilt))
        mapView.animate(to: camera)
    }
    
    
}

