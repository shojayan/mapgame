//
//  DialogCorrectAnswerVC.swift
//  MapGame
//
//  Created by omid shojaeian on 12/12/1397 AP.
//  Copyright © 1397 omid. All rights reserved.
//

import UIKit
import Spring


class DialogCorrectAnswerVC: UIViewController {

    
    @IBOutlet weak var starPoint1: UILabel!
    @IBOutlet weak var starPoint2: UILabel!
    @IBOutlet weak var starPoint3: UILabel!
    
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var qrcodePointLbl: UILabel!
    @IBOutlet weak var levelPointLbl: UILabel!
    @IBOutlet weak var totalPointLbl: UILabel!
    
    @IBOutlet weak var distance: NSLayoutConstraint!
    
    @IBOutlet weak var coponAvailableTxt: UILabel!
    @IBOutlet weak var showCoponBtn: BordredButon!
    
    let pointText:String = "امتیاز"
    let fillStar:String = ""
    let emptyStar:String = ""
    
    var coponCount:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.starPoint1.transform = CGAffineTransform(scaleX: 0, y: 0)
        self.starPoint2.transform = CGAffineTransform(scaleX: 0, y: 0)
        self.starPoint3.transform = CGAffineTransform(scaleX: 0, y: 0)
        prepareUI()
    }

    override func viewDidAppear(_ animated: Bool) {
        self.starsAnimating(isRepeat: false)
    }
    
    @IBAction func closeDialog(_ sender: BordredButon) {
        playButtonAnimation(view: sender)
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func showCoponAction(_ sender: BordredButon) {
        playButtonAnimation(view: sender)
        
    }
    func playButtonAnimation(view:SpringButton){
        view.animation = "pop"
        view.curve = "easeOutCirc"
        view.duration = 1.0
        view.animate()
    }
    func prepareUI(){
        
        if GamePlaySingleton.main.getMarkerInfo() != nil {
            qrcodePointLbl.text = "\(GamePlaySingleton.main.getMarkerInfo()!.barcodePoint ?? 0) " + pointText
            
            levelPointLbl.text = "\((GamePlaySingleton.main.getMarkerInfo()!.questionPoint ?? 0)  + (GamePlaySingleton.main.getMarkerInfo()!.bouncePoint ?? 0))" + pointText
            
            totalPointLbl.text = "\((GamePlaySingleton.main.getMarkerInfo()!.barcodePoint ?? 0) + (GamePlaySingleton.main.getMarkerInfo()!.questionPoint ?? 0) + (GamePlaySingleton.main.getMarkerInfo()!.bouncePoint ?? 0))" + pointText
            
            
            if GamePlaySingleton.main.getMarkerInfo()!.starRate == 0 {
                // 0 point
                starPoint1.text = emptyStar
                starPoint2.text = emptyStar
                starPoint3.text = emptyStar
            }else if GamePlaySingleton.main.getMarkerInfo()!.starRate! > 0.0 ,
                     GamePlaySingleton.main.getMarkerInfo()!.starRate! < 0.33 {
                // 1 point
                starPoint1.text = fillStar
                starPoint2.text = emptyStar
                starPoint3.text = emptyStar
            }else if GamePlaySingleton.main.getMarkerInfo()!.starRate! > 0.33 ,
                     GamePlaySingleton.main.getMarkerInfo()!.starRate! < 0.66 {
                //2 point
                starPoint1.text = fillStar
                starPoint2.text = fillStar
                starPoint3.text = emptyStar
            }else if GamePlaySingleton.main.getMarkerInfo()!.starRate! > 0.66{
             // 3 point
                starPoint1.text = fillStar
                starPoint2.text = fillStar
                starPoint3.text = fillStar
            }
            }
        
            if coponCount > 0 {
                distance.constant = 79
                coponAvailableTxt.text = "برای شما " + "\(coponCount) " + "کوپن جدید باز شد"
                coponAvailableTxt.isHidden = false
                showCoponBtn.isHidden = false
            }else {
                distance.constant = 8
                coponAvailableTxt.isHidden = true
                showCoponBtn.isHidden = true
        }
        }
    func starsAnimating(isRepeat:Bool){
        animationPopUp(viewAnimationg: starPoint1, scale: 1.25, timeduration: 0.1, delay: 0.0, complection: nil)
        animationPopUp(viewAnimationg: starPoint2, scale: 1.25, timeduration: 0.2, delay: 0.1, complection: nil)
        animationPopUp(viewAnimationg: starPoint3, scale: 1.25, timeduration: 0.2, delay: 0.2, complection: {
            // if need to repeat animation
            if isRepeat{
                self.starsAnimating(isRepeat: isRepeat)
            }
        })
    }
    func animationPopUp(viewAnimationg:UIView,scale:CGFloat,timeduration:TimeInterval,delay:TimeInterval,complection:(()->Void)?){
        
        UIView.animate(withDuration: timeduration, delay: delay, options: [.allowAnimatedContent], animations: {
            viewAnimationg.transform = CGAffineTransform(scaleX: scale, y: scale)
        }) { end  in
            UIView.animate(withDuration: timeduration, delay: 0, options: [.allowAnimatedContent], animations: {
                viewAnimationg.transform = CGAffineTransform(scaleX: 1, y: 1)
            }) { end  in
                complection?()
            }
        }
    }
    

    }

