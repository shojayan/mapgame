//
//  OffPageControllerVC.swift
//  MapGame
//
//  Created by omid shojaeian on 12/21/1397 AP.
//  Copyright © 1397 omid. All rights reserved.
//

import UIKit

class OffPageControllerVC: UIPageViewController,UIPageViewControllerDataSource,UIPageViewControllerDelegate {

    
    let webservice_getShopes:GetShopes = GetShopes()
    var shopesData:GetShopes.GetShopesModel?
    
    var subViewControllers:[OffVC] = [OffVC]()
    var currentIndex:Int = 0
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.backgroundColor = #colorLiteral(red: 0.1999762356, green: 0.200016588, blue: 0.1999709308, alpha: 1)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        removeSwipeGesture()
        self.dataSource = self
        self.delegate = self
        getShopes(complection: {
            if self.subViewControllers.count > 0 {
                self.setViewControllers([self.subViewControllers[0]],
                                        direction: UIPageViewController.NavigationDirection.forward,
                                        animated: true, completion: nil)
                self.postPagesCount()
            }
        })
        NotificationCenter.default.addObserver(self, selector: #selector(nextBack(_:)), name: NSNotification.Name("isNext"), object: nil)
    }
    @objc func nextBack(_ sender:Notification){
        if (sender.object as! Bool) {
            let currentIndex:Int = subViewControllers.index(of: self.viewControllers![0] as! OffVC) ?? 0
            print(currentIndex)
            if currentIndex == (self.subViewControllers.count - 1) {
                self.setViewControllers([self.subViewControllers[0]], direction: .reverse, animated: true, completion: { _ in
                    let currentIndex:Int = self.subViewControllers.index(of: self.viewControllers?[0] as! OffVC) ?? 0
                    print(currentIndex)
                    self.postCurrentPage(index: currentIndex)
                })
            }else {
                self.setViewControllers([self.subViewControllers[currentIndex + 1 ]], direction: .forward   , animated: true, completion: { _ in
                    let currentIndex:Int = self.subViewControllers.index(of: self.viewControllers?[0] as! OffVC) ?? 0
                    print(currentIndex)
                    self.postCurrentPage(index: currentIndex)
                })
            }
        }else {
            let currentIndex:Int = subViewControllers.index(of: self.viewControllers![0] as! OffVC) ?? 0
            print(currentIndex)
            if currentIndex == 0 {
                self.setViewControllers([self.subViewControllers[self.subViewControllers.count - 1 ]], direction: .reverse, animated: true, completion: { _ in
                    let currentIndex:Int = self.subViewControllers.index(of: self.viewControllers?[0] as! OffVC) ?? 0
                    print(currentIndex)
                    self.postCurrentPage(index: currentIndex)
                })
            }else {
                self.setViewControllers([self.subViewControllers[currentIndex - 1 ]], direction: .reverse   , animated: true, completion: { _ in
                    let currentIndex:Int = self.subViewControllers.index(of: self.viewControllers?[0] as! OffVC) ?? 0
                    print(currentIndex)
                    self.postCurrentPage(index: currentIndex)
                })
            }
        }
    }
        
    func postPagesCount(){
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "numberOfPages"), object: subViewControllers.count)
    }
    
    func createVC()->OffVC{
        return storyboard?.instantiateViewController(withIdentifier: "OffVC") as! OffVC
    }
    func postCurrentPage(index:Int){
        NotificationCenter.default.post(name: NSNotification.Name("currentPageNumber"), object: index)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func removeSwipeGesture(){
        for view in self.view.subviews {
            if let subView = view as? UIScrollView {
                subView.isScrollEnabled = false
            }
        }
    }
    func showLoadinfPager(isShow:Bool){
            self.loadingView(isShow: isShow,
                             type: .circleStrokeSpin,
                             font: font_IRANSansMobileFaNum(size: 10),
                             loadText: "درحال دریافت اطلاعات",
                             textColor: #colorLiteral(red: 1, green: 0.8198096156, blue: 0, alpha: 1),
                             backColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0),
                             shapeColor: #colorLiteral(red: 1, green: 0.8198096156, blue: 0, alpha: 1))
    }
    func getShopes(complection: @escaping ()->Void){
        self.showLoadinfPager(isShow: true)
        webservice_getShopes.getGetShops(compplection: { shopesData in
            DispatchQueue.main.async {
                self.showLoadinfPager(isShow: false)
                self.shopesData = shopesData
                for item in shopesData.extra! {
                    let vc = self.createVC()
                    vc.getShopItemData = item
                    self.subViewControllers.append(vc)
                }
                complection()
            }
        }) { errorString,errorIndex in
            
                self.showLoadinfPager(isShow: false)
                self.showCustomDialog(message: "خطایی رخ داده است", btnTitle: "دوباره تلاش کنید", completionSHow: nil, completionDismiss: {
                    self.getShopes(complection: {})
                })
            
        }
    }
    

    
    
    
    
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let currentIndex:Int = subViewControllers.index(of: viewController as! OffVC) ?? 0
        if currentIndex + 1 > subViewControllers.count - 1 {
            return subViewControllers[0]
        }
        
        return subViewControllers[currentIndex + 1 ]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let currentIndex:Int = subViewControllers.index(of: viewController as! OffVC) ?? 0
        if currentIndex - 1  < 0 {
            return subViewControllers[self.subViewControllers.count - 1 ]
            return nil
        }
        
        return subViewControllers[currentIndex - 1 ]
    }
    
    
    func  pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        let currentIndex:Int = subViewControllers.index(of: viewControllers?[0] as! OffVC) ?? 0
        print(currentIndex)
        postCurrentPage(index: currentIndex)
    }
    
}
