//
//  Dialog_AlertVC.swift
//  MapGame
//
//  Created by omid shojaeian on 12/19/1397 AP.
//  Copyright © 1397 omid. All rights reserved.
//

import UIKit

class Dialog_AlertVC: UIViewController {

    
    @IBOutlet weak var titleIcone: UILabel!
    @IBOutlet weak var textAlert: UILabel!
    @IBOutlet weak var button: BordredButon!
    typealias actionType = ()->Void
    enum actionsType {
        case button
    }
    var textAlertRecieved:String = ""
    var btnTitle:String = ""
    var actions:[actionsType:actionType] = [actionsType:actionType]()
    override func viewDidLoad() {
        super.viewDidLoad()
        textAlert.text = textAlertRecieved
        button.setTitle(btnTitle, for: .normal)
        button.setTitle(btnTitle, for: .selected)
        button.setTitle(btnTitle, for: .highlighted)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    
    @IBAction func buttonAction(_ sender: BordredButon) {
        if actions[.button] != nil {
            actions[.button]!()
        }
    }
    
}
