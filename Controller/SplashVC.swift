//
//  SplashVC.swift
//  MapGame
//
//  Created by omid shojaeian on 12/13/1397 AP.
//  Copyright © 1397 omid. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import SDWebImage

class SplashVC: UIViewController {
    
    @IBAction func unwindToSplash(segue: UIStoryboardSegue) {}
    let webservice_gamePlay:GamePlay = GamePlay()
    var isRegisterCompleted:Bool = false
    var webservice_RegisterUser = RegisterFcmToken()
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createSplash(isShow: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        splashLogical()
    }
    
    
    func splashLogical(){
        guard Reachability.isConnectedToNetwork() else {
            self.showCustomDialog(message: "تلفن شما به اینترنت متصل نمی باشد", btnTitle: "تلاش مجدد", completionSHow: nil) {
                self.splashLogical()
            }
            return
        }
        if isRegisterCompleted {
            self.getAllLevels()
        }else {
            if self.readRegisteredUserInfo() != nil {
                fcmToken(userRegistred: {
                    self.getAllLevels()
                }, userUnRegistred: {
                    let registerVC = self.storyboard?.instantiateViewController(withIdentifier: "RegisteringUserVC") as! RegisteringUserVC
                    DispatchQueue.main.async {
                        self.present(registerVC, animated: true, completion:nil)
                    }
                })
            }else {
                let registerVC = self.storyboard?.instantiateViewController(withIdentifier: "RegisteringUserVC") as! RegisteringUserVC
                DispatchQueue.main.async {
                    self.present(registerVC, animated: true, completion:nil)
                }
            }
        }
    }
    
    
    func createSplash(isShow:Bool){
        self.loadingView(isShow: isShow,
                         type: NVActivityIndicatorType.ballScaleMultiple,
                         font: font_IRANSansMobileFaNum(size: 10),
                         loadText: "در حال دریافت اطلاعات...",
                         textColor: #colorLiteral(red: 1, green: 0.8198096156, blue: 0, alpha: 1),
                         backColor: #colorLiteral(red: 0.1999762356, green: 0.200016588, blue: 0.1999709308, alpha: 1),
                         shapeColor: #colorLiteral(red: 1, green: 0.8198096156, blue: 0, alpha: 1))
    }
    
    func getAllLevels(){
        guard Reachability.isConnectedToNetwork() else {
            self.showCustomDialog(message: "تلفن شما به اینترنت متصل نمی باشد", btnTitle: "تلاش مجدد", completionSHow: nil) {
                self.getAllLevels()
            }
            return
        }
        webservice_gamePlay.getAllLevels(complection: { levelsData in
            guard let _ = levelsData.extra else {
                return
            }
            
            if levelsData.ok {
                GamePlaySingleton.main.updateSingleTone(levelesModel: levelsData)
                let registerVC = self.storyboard?.instantiateViewController(withIdentifier: "MainVC") as! MainVC
                DispatchQueue.main.async {
                    self.present(registerVC, animated: true, completion: nil)
                }
            }else {
                let registerVC = self.storyboard?.instantiateViewController(withIdentifier: "RegisteringUserVC") as! RegisteringUserVC
                DispatchQueue.main.async {
                    self.present(registerVC, animated: true, completion:nil)
                }
            }
            
        }) { errorString, errorIndex in
            
            self.showCustomDialog(message: "خطایی رخ داده است" + "\(errorIndex)",
                                  btnTitle: "تلاش مجدد",
                                  completionSHow: nil,
                                  completionDismiss: {
                                    self.getAllLevels()
            })
            
        }
    }
    
    
    
    func fcmToken(userRegistred:@escaping ()->Void,userUnRegistred:@escaping ()->Void)  {
        guard Reachability.isConnectedToNetwork() else {
            self.showCustomDialog(message: "تلفن شما به اینترنت متصل نمی باشد", btnTitle: "تلاش مجدد", completionSHow: nil) {
              self.splashLogical()
            }
            return
        }
        
        
        
        webservice_RegisterUser.registerUser(name: readRegisteredUserInfo()!.extra!.name!,
                                             familyName: readRegisteredUserInfo()!.extra!.family!,
                                             imageBase64: (self.readUserImage()).toBase64() ?? "",
                                             getError: { errorStr, errorInt in
                                                self.showCustomDialog(message: "خطایی رخ داده است",
                                                                      btnTitle: "تلاش مجدد",
                                                                      completionSHow: nil,
                                                                      completionDismiss: {
                                                                self.splashLogical()
                                                })
                                                
        }) { dataes in
            DispatchQueue.main.async {
                self.registerUser(info: dataes, userImage: self.readUserImage())
                if dataes.ok{
                    userRegistred()
                }else {
                    userUnRegistred()
                }
            }
        }
    }


}
