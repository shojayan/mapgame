//
//  MainOffVC.swift
//  MapGame
//
//  Created by omid shojaeian on 12/21/1397 AP.
//  Copyright © 1397 omid. All rights reserved.
//

import UIKit

class MainOffVC: UIViewController {

    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var pageNumber: UILabel!
    
    @IBOutlet weak var nextBtn: UIButton!
    
    @IBOutlet weak var backBtn: UIButton!
    var totalPage:Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        pageControl.numberOfPages = 0
        pageNumber.text = ""
        NotificationCenter.default.addObserver(self, selector: #selector(totalPages(_:)), name: NSNotification.Name(rawValue: "numberOfPages"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(currentPage(_:)), name: NSNotification.Name(rawValue: "currentPageNumber"), object: nil)
        nextBtn.isHidden = true
        backBtn.isHidden = true
    }
    

    @objc func totalPages(_ sender:Notification){
        nextBtn.isHidden = false
        backBtn.isHidden = false
        pageControl.numberOfPages = (sender.object as! Int)
        totalPage = (sender.object as! Int)
        pageNumber.text = "( " + "۱".convertToPersion() + " / " + "\(totalPage)".convertToPersion() + " )"
    }
    @objc func currentPage(_ sender:Notification){
        pageControl.currentPage = (sender.object as! Int)
        pageNumber.text = "( " + "\((sender.object as! Int) + 1)".convertToPersion() + " / " + "\(totalPage)".convertToPersion() + " )"
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        NotificationCenter.default.post(name: NSNotification.Name("isNext"), object: false)
    }
    
    
    @IBAction func nextAction(_ sender: UIButton) {
        NotificationCenter.default.post(name: NSNotification.Name("isNext"), object: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
