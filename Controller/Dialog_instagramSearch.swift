//
//  Dialog_instagramSearch.swift
//  MapGame
//
//  Created by omid shojaeian on 12/27/1397 AP.
//  Copyright © 1397 omid. All rights reserved.
//

import UIKit

class Dialog_instagramSearch: UIViewController {

    @IBOutlet weak var nameLevel: UILabel!
    @IBOutlet weak var instagramLbl: UILabel!
    @IBOutlet weak var codeInpute: UITextField!
    
    
    var levelName:String?
    var instagramText:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameLevel.text = levelName ?? ""
        instagramLbl.text = instagramText ?? ""
        prepareKeyBoard()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func prepareKeyBoard(){
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(hideKeyboard)))
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardAction(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardAction(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func hideKeyboard(){
        self.view.endEditing(true)
    }
    @objc func keyboardAction(_ sender:Notification){
        //let keyboardFrame = (sender.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        if sender.name == UIResponder.keyboardWillShowNotification{
            self.view.transform = CGAffineTransform(translationX: 0, y: -50)
        }else if sender.name == UIResponder.keyboardWillHideNotification {
            self.view.transform = CGAffineTransform(translationX: 0, y: 0)
        }
    }
    @IBAction func closeAction(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func sendCodeaction(_ sender: BordredButon) {
        found(code: codeInpute.text ?? "" )
    }
    
    @IBAction func searchingCodeAction(_ sender: BordredButon) {
        guard instagramText != nil  else {
            return
        }
        if instagramText!.hasPrefix("@"){
            openInstagram(id: instagramText!.replacingOccurrences(of: "@", with: "", options: String.CompareOptions.literal , range: nil), notInstalled: {
                self.showCustomDialog(message: "برنامه اینستاگرام نصب نمی باشد", btnTitle: "بستن", completionSHow: nil, completionDismiss: nil)
            })
        }else if instagramText!.hasPrefix("#"){
            openInstagramHashtag(hashtag: instagramText!.replacingOccurrences(of: "#", with: "", options: String.CompareOptions.literal , range: nil), notInstalled: {
                self.showCustomDialog(message: "برنامه اینستاگرام نصب نمی باشد", btnTitle: "بستن", completionSHow: nil, completionDismiss: nil)
            })
        }
        view.endEditing(true)
        
    }
    
    func found(code: String) {
        print(code)
        let getQuestion = GetQuestion()
            
            self.loadingViewForThisProject(isShow: true)
            getQuestion.getQuestion(levelId: GamePlaySingleton.main.getMarkerInfo()!.id!,
                                    barcode: code,
                                    barcodeType: .numberCode,
                                    getError: { errorStr, errorNum in
                                        self.showCustomDialog(message: "خطایی رخداده" + "\(errorNum)",
                                            btnTitle: "تلاش مجدد",
                                            completionSHow: nil,
                                            completionDismiss: {DispatchQueue.main.async {
                                                self.loadingViewForThisProject(isShow: false)
                                                }})
            }) { questionData in
                if questionData.ok {
                    DispatchQueue.main.async {
                        self.loadingViewForThisProject(isShow: false)
                        if let questionVC = self.storyboard?.instantiateViewController(withIdentifier: "Dialog_QuestionVC"){
                            (questionVC as! Dialog_QuestionVC).barcodeType = .numberCode
                            (questionVC as! Dialog_QuestionVC).code = code
                            if MapVCSingleton.main.mapVC != nil {
                                self.dismiss(animated: true) {
                                    MapVCSingleton.main.mapVC!.createGap(withDuration: 1.0,
                                                                         startGap: { mapVC  in
                                                                            mapVC.loadingViewForThisProject(isShow: true)
                                    }) { mapVC in
                                        mapVC.loadingViewForThisProject(isShow: false)
                                        mapVC.present(questionVC, animated: true, completion: nil)
                                    }
                                }
                            }else {
                                self.showCustomDialog(message: "مشکل در نرم افزار - نرم افزار را بسته و دوباره اجرا کنید", btnTitle: "بستن", completionSHow: nil , completionDismiss: nil )
                            }
                        }
                    }
                }else{
                    DispatchQueue.main.async {
                        self.loadingViewForThisProject(isShow: false)
                        self.showCustomDialog(message: "بار کد اشتباه می باشد",
                                              btnTitle: "تلاش مجدد",
                                              completionSHow: nil,
                                              completionDismiss: {
                                                
                        })
                    }
                }
            }
        
        
        
    }
    
}
