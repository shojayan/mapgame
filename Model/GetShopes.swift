//
//  GetShopes.swift
//  MapGame
//
//  Created by omid shojaeian on 12/20/1397 AP.
//  Copyright © 1397 omid. All rights reserved.
//

import Foundation


class GetShopes:SuperModel {
    
    struct GetShopesModel:Codable {
        var ok:Bool
        var messages:[MessageViewModelItem]?
        var extra:[GetShopes_extraItem]?
    }
    
    struct GetShopes_extraItem:Codable {
        var id:Int
        var fullName:String?
        var address:String?
        var lat:Double?
        var lang:Double?
        var phoneNumber:String?
        var imageUrl:String?
        var description:String?
        var discounts:[GetShopes_extraItem_discountsItem]?
    }
    
    struct GetShopes_extraItem_discountsItem:Codable {
        var id:Int?
        var title:String?
        var minPurchaseAmount:Int?
        var maxPurchaseDiscount:Int?
        var creditTime:String?//": "2020/03/10 00:00:00",
        var discountPercent:Int?
        var minScore:Int?
        var uniqueCode:String?
        var shopId:Int?
    }
 
    func getGetShops(compplection:@escaping (GetShopesModel)->Void ,
                     errorComplection:@escaping (String,Int)->Void ){
        let headers = ["DeviceId": deviceID()!]
        
        let request = NSMutableURLRequest(url: NSURL(string: "\(superURL)/api/shop/GetShops")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                errorComplection("internet error eccured ",4 )
            } else {
                let httpResponse = response as? HTTPURLResponse
                let decoder = JSONDecoder()
                if httpResponse != nil {
                    if httpResponse!.statusCode == 200 {
                        let result = try? decoder.decode(GetShopesModel.self, from: data!)
                        if result != nil {
                            compplection(result!)
                        }else {
                            errorComplection("can not pars jason ",3)
                        }
                        }else {
                        errorComplection("other response ",2)
                    }
                }else {
                    errorComplection("httpResponse is nil ",1)
                }
            }
        })
        
        dataTask.resume()
    }
}
