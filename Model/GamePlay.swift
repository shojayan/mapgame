//
//  GamePlay.swift
//  MapGame
//
//  Created by omid shojaeian on 12/7/1397 AP.
//  Copyright © 1397 omid. All rights reserved.
//

import Foundation
import UIKit

class GamePlay:SuperModel {
    
    struct GamePlayLeveleModel:Codable {
        var ok:Bool
        var messages:[MessageViewModelItem]?
        var extra:[GamePlayLevelesModel_extra_item]?
    }
    
    struct GamePlayLevelesModel_extra_item:Codable {
        var id:Int?
        var name:String?
        var description:String?
        var barcodePoint:Int?
        var questionPoint:Int?
        var order:Int?
        var starRate:Double?
        var bouncePoint:Int?
        var barcode:GamePlayLevelesModel_extra_item_barcode?
    }
    struct GamePlayLevelesModel_extra_item_barcode:Codable {
        var id:Int?
        var description:String?
        var address:String?
        var lat:Double?
        var lang:Double?
        var help:String?
        var order:Int?
        //var uniqueCode:String?
        //var numericalUniqueCode:Int? // barcode
        var uniqueCodeHelp:String?
        var question:GamePlayLevelesModel_extra_item_barcode_question?
    }
    struct GamePlayLevelesModel_extra_item_barcode_question:Codable {
        var id :Int?
        var body :String?
        var imageUrl :String?
        var minTime :Int?
        var maxTime :Int?
        var alfa :Float?
        var answerStatus :Int?
        var answers :[GamePlayLevelesModel_extra_item_barcode_question_answers]?
    }
    struct GamePlayLevelesModel_extra_item_barcode_question_answers:Codable {
        var id:Int?
        var body:String?
        var imageUrl:String?
        var isAnswer:Bool?
    }
    
    
    func getAllLevels(complection:@escaping (GamePlayLeveleModel)->Void,
                      otherErrorsCode:@escaping (String,Int)->Void){
        guard let _ = deviceID() else {
            return
        }
        let headers = [
            "DeviceId": deviceID()!,
            "Authorization": "Bearer"
        ]
        
        let request = NSMutableURLRequest(url: NSURL(string: "\(superURL)/api/register/GamePlay")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                otherErrorsCode("webservice getAllLevels error 4",4)
            } else {
                let httpResponse = response as? HTTPURLResponse
                let decoder = JSONDecoder()
                let result = try? decoder.decode(GamePlayLeveleModel.self, from: data!)
                if httpResponse != nil {
                    if httpResponse!.statusCode == 200 || httpResponse!.statusCode == 400 {
                        if result != nil {
                            complection(result!)
                        }else{
                            otherErrorsCode("webservice getAllLevels error 3",3)
                        }
                    }else {
                        otherErrorsCode("webservice getAllLevels error 2",2)
                    }
                }else {
                    otherErrorsCode("webservice getAllLevels error 1",1)
                }
            }
        })
        dataTask.resume()
    }
}

