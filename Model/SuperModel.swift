//
//  SuperModel.swift
//  MapGame
//
//  Created by omid shojaeian on 12/7/1397 AP.
//  Copyright © 1397 omid. All rights reserved.
//

import Foundation
import UIKit

class SuperModel  {
    let superURL = "http://178.216.248.90/sarnakh"
    struct MessageViewModelItem:Codable {
        var code:Int?
        var description:String?
    }
    func deviceID()->String? {
        return (UIDevice.current.identifierForVendor?.uuidString)!
    }
    
    func getSystemVersion()->String {
        return  UIDevice.current.systemVersion
    }
    
    func getAppVersion()->String{
        return Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
    }
    func convertStringToURL(str:String)->URL?{
        let str2 = str.replacingOccurrences(of: "ك", with: "ک").replacingOccurrences(of: "ي", with: "ی").addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        return URL(string: str2!)
    }
}
