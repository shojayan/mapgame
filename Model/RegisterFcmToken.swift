//
//  RegisterFcmToken.swift
//  MapGame
//
//  Created by omid shojaeian on 12/13/1397 AP.
//  Copyright © 1397 omid. All rights reserved.
//

import Foundation
import UIKit
import FirebaseMessaging

class RegisterFcmToken: SuperModel {
    
    
    struct UserTokenViewModel:Codable {
        var ok :Bool
        var messages :[MessageViewModelItem]?
        var extra : UserTokenViewModel_extra?
    }
    
    
    struct UserTokenViewModel_extra:Codable {
        var name :String?
        var family :String?
        var imageUrl :String?
        var userScore:Int?// (integer, optional): امتیاز کاربر ,
        var userRate:Int?// (integer, optional): رتبه کاربر
    }
    
    func registerUser(name:String,
                      familyName:String,
                      imageBase64:String,
                      getError:@escaping (String,Int)->Void,
                      completion:@escaping (UserTokenViewModel)->Void) {
        
        guard Messaging.messaging().fcmToken != nil  else {
            getError("registerUser fcm tokec erro 5" , 5)
            return
        }
        
        let headers = [
            "DeviceId": deviceID()!,
            "Content-Type": "application/json"
        ]
        let parameters = [
            "deviceModel": UIDevice.modelName,
            "deviceId": deviceID()!,
            "appVersion": getAppVersion(),
            "fcmToken": Messaging.messaging().fcmToken!,
            "osType": "Ios",
            "osVersion": getSystemVersion(),
            "jwtToken": "jwtToken-string",
            "name": name,
            "family": familyName,
            "imageUrl": imageBase64
            ] as [String : Any]
        
        let postData = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        
        let request = NSMutableURLRequest(url: NSURL(string: "\(superURL)/api/register/FcmToken")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error)
                getError("webservice registerUser error 4",4)
            } else {
                let httpResponse = response as? HTTPURLResponse
                let decoder = JSONDecoder()
                let result = try? decoder.decode(UserTokenViewModel.self, from: data!)
                
                if httpResponse != nil {
                    if httpResponse!.statusCode == 200 {
                        if result != nil {
                            completion(result!)
                        }else {
                            getError("webservice registerUser error 3",3)
                        }
                    }else{
                        getError("webservice registerUser error 2",2)
                    }
                }else{
                    getError("webservice registerUser error 1",1)
                }
            }
        })
        
        dataTask.resume()
    }
}
