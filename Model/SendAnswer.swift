//
//  SendAnswer.swift
//  MapGame
//
//  Created by omid shojaeian on 12/9/1397 AP.
//  Copyright © 1397 omid. All rights reserved.
//

import Foundation



class SendAnswer:SuperModel{
    
    struct AnswerModel:Codable {
        var ok:Bool
        var messages:[MessageViewModelItem]?
        var extra:AnswerModel_extra?
    }
    struct AnswerModel_extra:Codable {
        var couponCount:Int?
        var level:AnswerModel_extra_level?
    }
    struct AnswerModel_extra_level:Codable {
        var id:Int?
        var name:String?
        var description:String?
        var barcodePoint:Int?
        var questionPoint:Int?
        var order:Int?
        var starRate:Double?
        var bouncePoint:Int?
        var barcode:AnswerModel_extra_level_barcode?
    }
    struct AnswerModel_extra_level_barcode:Codable {
        var id:Int?
        var description:String?
        var address:String?
        var lat:Float?
        var lang:Float?
        var help:String?
        var order:Int?
        var uniqueCode:String?
        var numericalUniqueCode:Int? // barcode
        var question:AnswerModel_extra_level_barcode_question?
    }
    struct AnswerModel_extra_level_barcode_question:Codable {
        var id :Int?
        var body :String?
        var imageUrl :String?
        var minTime :Int?
        var maxTime :Int?
        var alfa :Float?
        var answerStatus :Int?
        var help:String?
        var answers :[AnswerModel_extra_level_barcode_question_answerItem]?
    }
    struct AnswerModel_extra_level_barcode_question_answerItem:Codable {
        var id:Int?
        var body:String?
        var imageUrl:String?
        var isAnswer:Bool?
    }
    
    
    func getQuestion(questionId:Int,answerId:Int,
                     getError:@escaping (String,Int)->Void,
                     completion:@escaping (AnswerModel)->Void){
        
        let headers = [
            "DeviceId": deviceID()!,
            "Authorization": "Bearer"
        ]
        
        let request = NSMutableURLRequest(url: NSURL(string: "\(superURL)/api/question/SendAnswer?questionId=\(questionId)&answerId=\(answerId)")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                getError("webservice getQuestion error 4",4)
            } else {
                let httpResponse = response as? HTTPURLResponse
                let decoder = JSONDecoder()
                let result = try? decoder.decode(AnswerModel.self, from: data!)
                
                if httpResponse != nil {
                    if httpResponse!.statusCode == 200 {
                        if result != nil {
                            completion(result!)
                        }else {
                            getError("webservice getQuestion error 3",3)
                        }
                    }else{
                        getError("webservice getQuestion error 2",2)
                    }
                }else{
                    getError("webservice getQuestion error 1",1)
                }
            }
        })
        
        dataTask.resume()
    }
}
