//
//  GetQuestion.swift
//  MapGame
//
//  Created by omid shojaeian on 12/9/1397 AP.
//  Copyright © 1397 omid. All rights reserved.
//

import Foundation



class GetQuestion:SuperModel{
    
    struct QuestionModel:Codable {
        var ok:Bool
        var messages:[MessageViewModelItem]?
        var extra:QuestionModelExtra?
    }
    
    struct QuestionModelExtra:Codable {
        var id:Int?
        var body:String?
        var imageUrl:String?
        var minTime:Int?
        var maxTime:Int?
        var alfa:Float?
        var help:String?
        var answers:[QuestionModelExtra_answerItem]?
    }
    
    struct QuestionModelExtra_answerItem:Codable {
        
        var id:Int?
        var body:String?
        var imageUrl:String?
        var isAnswer:Bool?
        
    }
    enum BarCodeType {
        case numberCode
        case uniqCode
    }
    
    func getQuestion(levelId:Int,barcode:String,
                     barcodeType:BarCodeType,
                     getError:@escaping (String,Int)->Void,
                     completion:@escaping (QuestionModel)->Void){
        
        let headers = [
            "DeviceId": deviceID()!,
            "Authorization": "Bearer"
        ]
        //numericalBarcode
        //barcode
        var request:NSMutableURLRequest!
        if barcodeType == .numberCode {
            request = NSMutableURLRequest(url: convertStringToURL(str: "\(superURL)/api/question/GetQuestion?levelId=\(levelId)&numericalBarcode=\(barcode)")!,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        }else if barcodeType == .uniqCode {
            request = NSMutableURLRequest(url: convertStringToURL(str: "\(superURL)/api/question/GetQuestion?levelId=\(levelId)&barcode=\(barcode)")!,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        }
        
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                getError("webservice getQuestion error 4",4)
            } else {
                let httpResponse = response as? HTTPURLResponse
                let decoder = JSONDecoder()
                let result = try? decoder.decode(QuestionModel.self, from: data!)
                
                if httpResponse != nil {
                    if httpResponse!.statusCode == 200 {
                        if result != nil {
                            completion(result!)
                        }else {
                            getError("webservice getQuestion error 3",3)
                        }
                    }else{
                        getError("webservice getQuestion error 2",2)
                    }
                }else{
                    getError("webservice getQuestion error 1",1)
                }
            }
        })
        
        dataTask.resume()
    }
}
