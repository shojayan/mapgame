//
//  GetLeaderShip.swift
//  MapGame
//
//  Created by omid shojaeian on 12/22/1397 AP.
//  Copyright © 1397 omid. All rights reserved.
//

import Foundation


class GetLeaderShip:SuperModel {
    
    
    struct GetLeaderShipModel:Codable{
        var ok:Bool
        var messages:[MessageViewModelItem]?
        var extra:[GetLeaderShipModel_extraItem]?
    }
        
        struct GetLeaderShipModel_extraItem:Codable{
            var name:String?
            var family:String?
            var imageUrl:String?
            var userScore:Int?
        }
        
    func getAllLeaderShip(getError:@escaping (String,Int)->Void ,
                          completion:@escaping (GetLeaderShipModel)->Void ){
        guard deviceID() != nil else {
            getError("device Id not found 5" , 5)
            return
        }
        let headers = [
            "Authorization": "Bearer",
            "DeviceId": deviceID()!
        ]
        
        let request = NSMutableURLRequest(url: NSURL(string: "\(superURL)/api/score/GetLeaderShip")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                getError("internert error 4 ",4)
            } else {
                let httpResponse = response as? HTTPURLResponse
                let decoder = JSONDecoder()
                let result = try? decoder.decode(GetLeaderShipModel.self , from: data!)
                
                if httpResponse != nil{
                    if httpResponse!.statusCode == 200 {
                        if result != nil {
                            completion(result!)
                        }else {
                            getError("parsing error  3 ",3)
                        }
                    }else {
                        getError("other response number 2 ",2)
                    }
                }else {
                    getError("response is nil 1 ",1)
                }
            }
        })
        
        dataTask.resume()
    }
    
    
    func getLeaderShipBy(date:String,getError:@escaping (String,Int)->Void ,
                         completion:@escaping (GetLeaderShipModel)->Void ){
        guard deviceID() != nil else {
            getError("device Id not found 5" , 5)
            return
        }
        let headers = [
            "Authorization": "Bearer",
            "DeviceId": deviceID()!
        ]
        
        let request = NSMutableURLRequest(url: NSURL(string: "\(superURL)/api/score/GetLeaderShip?correctDate=\(date)")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                getError("internert error 4 ",4)
            } else {
                let httpResponse = response as? HTTPURLResponse
                let decoder = JSONDecoder()
                let result = try? decoder.decode(GetLeaderShipModel.self , from: data!)
                
                if httpResponse != nil{
                    if httpResponse!.statusCode == 200 {
                        if result != nil {
                            completion(result!)
                        }else {
                            getError("parsing error  3 ",3)
                        }
                    }else {
                        getError("other response number 2 ",2)
                    }
                }else {
                    getError("response is nil 1 ",1)
                }
            }
        })
        
        dataTask.resume()
    }
}
