//
//  Extentions.swift
//  MediaRasa2
//
//  Created by omid on 7/24/1397 AP.
//  Copyright © 1397 omid. All rights reserved.
//

import Foundation
import UIKit
//import NVActivityIndicatorView


extension UIViewController {
//    func changeBackButtonwith(title:String,imageIcone:UIImage?){
//        self.navigationController?.navigationBar.backIndicatorImage = imageIcone != nil ? imageIcone:UIImage()
//        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = imageIcone != nil ? imageIcone:UIImage()
//        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: title, style: UIBarButtonItem.Style.done, target: nil, action: nil)
//    }
    func deviceID()->String? {
        return UIDevice.current.identifierForVendor?.uuidString
    }
    
    
    func font_IRANSansMobileFaNum(size:CGFloat)->UIFont {
        return UIFont(name: "IRANSansMobileFaNum", size: size)!
    }
    func font_IRANSansMobileFaNum_Bold(size:CGFloat)->UIFont {
        return UIFont(name: "IRANSansMobileFaNum-Bold", size: size)!
    }
    func font_IRANSansMobileFaNum_Light(size:CGFloat)->UIFont{
        return UIFont(name: "IRANSansMobileFaNum-Light", size: size)!
    }
    func font_MapGame(size:CGFloat)->UIFont {
        return UIFont(name: "MapGame", size: size)!
    }
    
    
    
    
    func showLoadingView(isShow:Bool){
        if isShow{
            if self.view.viewWithTag(100) == nil {
                let loadingContainer = UIView(frame: UIScreen.main.bounds)
                loadingContainer.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5)
                loadingContainer.layer.zPosition = 100
                loadingContainer.alpha = 0
                loadingContainer.tag = 100
                
                let boxView = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
                boxView.center = CGPoint(x: loadingContainer.bounds.width / 2 , y: loadingContainer.bounds.height / 2)
                boxView.backgroundColor = #colorLiteral(red: 0.7272565038, green: 0.7272565038, blue: 0.7272565038, alpha: 0.4426637414)
                boxView.tag = 130
                boxView.layer.masksToBounds = true
                boxView.layer.cornerRadius = 5
                
                let activityIndicator = UIActivityIndicatorView(style: .whiteLarge)
                activityIndicator.center = CGPoint(x: boxView.bounds.width / 2, y: boxView.bounds.height / 2)
                activityIndicator.startAnimating()
                activityIndicator.tag = 150
                boxView.addSubview(activityIndicator)
                
                loadingContainer.addSubview(boxView)
                self.view.addSubview(loadingContainer)
                
                UIView.animate(withDuration: 0.5) {
                    loadingContainer.alpha = 1
                }
            }else{
                
            }
            
        }else{
            UIView.animate(withDuration: 0.5, animations: {
                self.view.viewWithTag(100)?.alpha = 0
            }) { state in
                (self.view.viewWithTag(100)?.viewWithTag(130)?.viewWithTag(150) as? UIActivityIndicatorView)?.stopAnimating()
                self.view.viewWithTag(100)?.removeFromSuperview()
            }
        }
    }
    func createCancellableGeneralAlert(title:String,message:String,cancelTitle:String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let alertCancelAction = UIAlertAction(title: cancelTitle, style: .cancel, handler: nil)
        alert.addAction(alertCancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    func createCancellableGeneralActionAlert(title:String,message:String,cancelTitle:String,action:@escaping (UIAlertController)->Void){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let alertCancelAction = UIAlertAction(title: cancelTitle, style: .cancel) { alertAction in
            action(alert)
        }
        alert.addAction(alertCancelAction)
        self.present(alert, animated: true, completion: nil)
    }
}


extension String {
    func stringToUTF16String (str:String) -> String {
        
        var retval:String = str
        retval = retval.replacingOccurrences(of: "0", with: "۰", options: .literal, range: nil)
        retval = retval.replacingOccurrences(of: "1", with: "۱", options: .literal, range: nil)
        retval = retval.replacingOccurrences(of: "2", with: "۲", options: .literal, range: nil)
        retval = retval.replacingOccurrences(of: "3", with: "۳", options: .literal, range: nil)
        retval = retval.replacingOccurrences(of: "4", with: "۴", options: .literal, range: nil)
        retval = retval.replacingOccurrences(of: "5", with: "۵", options: .literal, range: nil)
        retval = retval.replacingOccurrences(of: "6", with: "۶", options: .literal, range: nil)
        retval = retval.replacingOccurrences(of: "7", with: "۷", options: .literal, range: nil)
        retval = retval.replacingOccurrences(of: "8", with: "۸", options: .literal, range: nil)
        retval = retval.replacingOccurrences(of: "9", with: "۹", options: .literal, range: nil)
        return retval
    }
    func convertPersionToEngilish(str:String)->String{
        var retval:String = str
        retval = retval.replacingOccurrences(of: "۰", with: "0", options: .literal, range: nil)
        retval = retval.replacingOccurrences(of: "۱", with: "1", options: .literal, range: nil)
        retval = retval.replacingOccurrences(of: "۲", with: "2", options: .literal, range: nil)
        retval = retval.replacingOccurrences(of: "۳", with: "3", options: .literal, range: nil)
        retval = retval.replacingOccurrences(of: "۴", with: "4", options: .literal, range: nil)
        retval = retval.replacingOccurrences(of: "۵", with: "5", options: .literal, range: nil)
        retval = retval.replacingOccurrences(of: "۶", with: "6", options: .literal, range: nil)
        retval = retval.replacingOccurrences(of: "۷", with: "7", options: .literal, range: nil)
        retval = retval.replacingOccurrences(of: "۸", with: "8", options: .literal, range: nil)
        retval = retval.replacingOccurrences(of: "۹", with: "9", options: .literal, range: nil)
        return retval
    }
    func convertToPersion()->String{
        return stringToUTF16String(str: self)
    }
}
extension UIApplication {
    
    class var topViewController: UIViewController? {
        return getTopViewController()
    }
    
    private class func getTopViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return getTopViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return getTopViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return getTopViewController(base: presented)
        }
        return base
    }
}

extension Equatable {
    func share() {
        let activity = UIActivityViewController(activityItems: [self], applicationActivities: nil)
        UIApplication.topViewController?.present(activity, animated: true, completion: nil)
    }
}
