//
//  BordredButon.swift
//  MapGame
//
//  Created by omid on 11/29/1397 AP.
//  Copyright © 1397 omid. All rights reserved.
//

import UIKit


@IBDesignable
class BordredButon: UIButton {

    
    @IBInspectable var borderColor:UIColor = UIColor.black
    @IBInspectable var borderWidth:CGFloat = 1
    
    
    let textLayer = CATextLayer()
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        layer.cornerRadius = rect.height / 2
        layer.masksToBounds = true
        layer.borderColor = borderColor.cgColor
        layer.borderWidth = borderWidth
    }
 
}
