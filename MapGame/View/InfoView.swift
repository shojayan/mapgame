//
//  InfoView.swift
//  MapGame
//
//  Created by omid on 12/1/1397 AP.
//  Copyright © 1397 omid. All rights reserved.
//

import UIKit


@IBDesignable
class InfoView: UIView {

    
    @IBInspectable var distance:CGFloat = 20
    @IBInspectable var h_distance:CGFloat = 30
    @IBInspectable var gapDistance:CGFloat = 50
    @IBInspectable var gapHeight:CGFloat = 20
    @IBInspectable var strockColor:UIColor = UIColor.yellow
    @IBInspectable var fillColor:UIColor = UIColor.yellow
     
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        
        let shaplayer = CAShapeLayer()
        shaplayer.strokeColor = strockColor.cgColor
        shaplayer.fillColor = fillColor.cgColor
        
        let path = UIBezierPath()
        
        
        path.move(to: CGPoint(x: distance, y: 0))
        path.addLine(to: CGPoint(x: rect.width - distance , y: 0 ))
        path.addCurve(to: CGPoint(x: rect.width - distance , y: rect.height ),
                      controlPoint1: CGPoint(x: (rect.width - distance) + h_distance, y: 0),
                      controlPoint2: CGPoint(x: (rect.width - distance) + h_distance, y: rect.height))
        
        path.addLine(to: CGPoint(x:  gapDistance, y: rect.height))
        path.addLine(to: CGPoint(x:  gapDistance - (gapHeight / 2) , y: rect.height + gapHeight))
        path.addLine(to: CGPoint(x:  (gapDistance - gapHeight), y: rect.height))
        path.addLine(to: CGPoint(x: distance , y: rect.height))
        
        
        path.addCurve(to: CGPoint(x: distance , y: 0 ),
                      controlPoint1: CGPoint(x: distance - h_distance, y: rect.height),
                      controlPoint2: CGPoint(x: distance - h_distance, y: 0))
        
        path.close()
        shaplayer.backgroundColor = UIColor.clear.cgColor
        shaplayer.path = path.cgPath
        
        layer.addSublayer(shaplayer)
    }
 

}
