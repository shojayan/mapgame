//
//  BorderView.swift
//  MapGame
//
//  Created by omid on 11/29/1397 AP.
//  Copyright © 1397 omid. All rights reserved.
//

import UIKit


@IBDesignable
class BorderView: UIView {

    
    @IBInspectable var h_space:CGFloat = 30
    @IBInspectable var distance:CGFloat = 30
    @IBInspectable var radiouse:CGFloat = 60
    @IBInspectable var lineWidth:CGFloat = 8
    
    @IBInspectable var strokeColor:UIColor = UIColor.yellow
    @IBInspectable var fillColor:UIColor = UIColor.white
    
    @IBInspectable var horizontalLineIsShowHorizontalLine:Bool = true
    @IBInspectable var horizontalLineWidthSize:CGFloat = 2
    @IBInspectable var horizontalLineX:CGFloat = 50
    @IBInspectable var horizontalLineY:CGFloat = 50
    @IBInspectable var horizontalLineDistance:CGFloat = 50
    
    @IBInspectable var centerLineIsShowCenterLine:Bool = true
    @IBInspectable var centerLineWidthSize:CGFloat = 2
    var centerLineX:CGFloat = 10
    @IBInspectable var centerLineY:CGFloat = 100
    @IBInspectable var centerLineDistance:CGFloat = 100
    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        createBorder(rect:rect)
        createHorizontalLine(rect:rect)
        createCenterLine(rect:rect)
    }
    

    func createBorder(rect:CGRect){
        let shapeLaye = CAShapeLayer()
        shapeLaye.frame = rect
        shapeLaye.lineWidth = lineWidth
        shapeLaye.strokeColor = strokeColor.cgColor
        shapeLaye.fillColor = fillColor.cgColor
        shapeLaye.strokeStart = 0
        shapeLaye.strokeEnd = 1
        shapeLaye.lineJoin = .round
        
        
        let centerWidth:CGFloat = rect.width / 2
        let path = UIBezierPath()
        path.move(to: CGPoint(x: distance, y: 0))
        ///////////
        path.addLine(to: CGPoint(x: centerWidth - radiouse, y: 0))
        ///////////
        path.addArc(withCenter: CGPoint(x: centerWidth, y: 0),
                    radius: radiouse,
                    startAngle: CGFloat.pi,
                    endAngle: 0,
                    clockwise: true)
        
        path.addLine(to: CGPoint(x: rect.width - distance ,
                                 y: 0))
        ///////
        path.addCurve(to: CGPoint(x: rect.width,
                                  y: distance ),
                      controlPoint1: CGPoint(x: rect.width - distance + h_space ,
                                             y: 0),
                      controlPoint2: CGPoint(x: rect.width,
                                             y: distance - h_space))
        ///////////
        path.addLine(to: CGPoint(x: rect.width, y: rect.height - distance))
        ///////////
        path.addCurve(to: CGPoint(x: rect.width - distance,
                                  y: rect.height ),
                      controlPoint1: CGPoint(x: rect.width  ,
                                             y: rect.height - distance + h_space),
                      controlPoint2: CGPoint(x: rect.width - distance + h_space,
                                             y: rect.height))
        ///////////
        path.addLine(to: CGPoint(x: distance , y: rect.height))
        ///////////
        path.addCurve(to: CGPoint(x: 0, y: rect.height - distance ),
                      controlPoint1: CGPoint(x: distance - h_space,
                                             y: rect.height ),
                      controlPoint2: CGPoint(x: 0,
                                             y: rect.height - distance + h_space))
        ///////////
        path.addLine(to: CGPoint(x: 0, y: distance))
        ///////////
        path.addCurve(to: CGPoint(x: distance ,
                                  y: 0 ),
                      controlPoint1: CGPoint(x: 0 ,
                                             y: distance - h_space  ),
                      controlPoint2: CGPoint(x: distance - h_space  ,
                                             y: 0))
        ///////////
        path.close()
        shapeLaye.path = path.cgPath
        self.layer.addSublayer(shapeLaye)
    }
    
    
    
    func createHorizontalLine (rect:CGRect){
        if horizontalLineIsShowHorizontalLine {
            let underLineLayer = CAShapeLayer()
            underLineLayer.strokeColor = strokeColor.cgColor
            underLineLayer.fillColor = fillColor.cgColor
            underLineLayer.frame = rect
            underLineLayer.lineWidth = horizontalLineWidthSize
            
            let underlinePath = UIBezierPath()
            underlinePath.move(to: CGPoint(x: horizontalLineX,
                                           y: rect.height - horizontalLineY))
            
            underlinePath.addLine(to: CGPoint(x: horizontalLineX + horizontalLineDistance,
                                              y: rect.height - horizontalLineY))
            
            underLineLayer.path = underlinePath.cgPath
            
            self.layer.addSublayer(underLineLayer)
        }
    }
    
    
    func createCenterLine(rect:CGRect){
        if centerLineIsShowCenterLine {
            let vLLayer = CAShapeLayer()
            vLLayer.strokeColor = strokeColor.cgColor
            vLLayer.fillColor = fillColor.cgColor
            vLLayer.frame = rect
            vLLayer.lineWidth = centerLineWidthSize
            centerLineX = rect.width / 2
            let vLPath = UIBezierPath()
            vLPath.move(to: CGPoint(x: rect.width / 2 ,
                                           y: centerLineY))
            
            vLPath.addLine(to: CGPoint(x: rect.width / 2 ,
                                              y: centerLineY + centerLineDistance))
            
            vLLayer.path = vLPath.cgPath
            
            self.layer.addSublayer(vLLayer)
        }
    }

}
