//
//  MaskView.swift
//  MapGame
//
//  Created by omid on 11/28/1397 AP.
//  Copyright © 1397 omid. All rights reserved.
//

import UIKit

@IBDesignable
class MaskView: UIView {

    
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
     
     let maskLayer = CAShapeLayer()
     maskLayer.frame = rect
     // Create the frame for the circle.
     //let radius: CGFloat = 50.0
     // Rectangle in which circle will be drawn
     //var rect = CGRect(x: 100, y: 100, width: 2 * radius, height: 2 * radius)
     let viewCenter = CGPoint(x: rect.width / 2 , y: rect.height / 2)
     let masksize = CGSize(width: rect.width - 100, height: rect.width - 100)
     let maskorigin = CGPoint(x: viewCenter.x - masksize.width / 2 ,
                              y: viewCenter.y - masksize.height / 2)
     let maskRect = CGRect(origin: maskorigin, size: masksize)
     //let circlePath = UIBezierPath(ovalIn: rect)
     let circlePath = UIBezierPath(roundedRect: maskRect, cornerRadius: 10)
     // Create a path
     let path = UIBezierPath(rect: self.bounds)
     // Append additional path which will create a circle
     path.append(circlePath)
     // Setup the fill rule to EvenOdd to properly mask the specified area and make a crater
     maskLayer.fillRule = CAShapeLayerFillRule.evenOdd
     // Append the circle to the path so that it is subtracted.
     maskLayer.path = path.cgPath
     // Mask our view with Blue background so that portion of red background is visible
     self.layer.mask = maskLayer
    }
 

}
