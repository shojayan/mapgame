//
//  RegisterUserVC.swift
//  MapGame
//
//  Created by omid on 12/1/1397 AP.
//  Copyright © 1397 omid. All rights reserved.
//

import UIKit

class RegisterUserVC: UIViewController {

    
    
    @IBOutlet var StepsLbl: [UILabel]!
    @IBOutlet weak var sendButton: BordredButon!
    @IBOutlet weak var sendLbl:UILabel!
    @IBOutlet weak var mobileNumberField: UITextField!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareKeyBoard()
        StepsLbl.forEach { lable in
            lable.layer.borderColor = #colorLiteral(red: 1, green: 0.8233510256, blue: 0, alpha: 1)
            lable.layer.borderWidth = 1
        }
        sendButton.addTarget(self, action: #selector(scanBarcod(_:)), for: .touchUpInside)
        sendButton.addTarget(self, action: #selector(scanBarcod2(_:)), for: .touchDown)
    }
    
    func prepareKeyBoard(){
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(hideKeyboard)))
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardAction(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardAction(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func hideKeyboard(){
        self.view.endEditing(true)
    }
    @objc func keyboardAction(_ sender:Notification){
        let keyboardFrame = (sender.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        if sender.name == UIResponder.keyboardWillShowNotification{
            self.view.transform = CGAffineTransform(translationX: 0, y: -keyboardFrame.height)
        }else if sender.name == UIResponder.keyboardWillHideNotification {
            self.view.transform = CGAffineTransform(translationX: 0, y: 0)
        }
    }
    
    
    @objc func scanBarcod(_ sender:BordredButon){
       
            UIView.animate(withDuration: 0.3) {
                self.sendLbl.alpha = 1
            }
        
        
    }
    @objc func scanBarcod2(_ sender:BordredButon){
            self.sendLbl.alpha = 0.3
    }
    @IBAction func sendAction(_ sender: BordredButon) {
    }
    
    @IBAction func register_logIn_Action(_ sender: UIButton) {
    }
    
    @IBAction func backAction(_ sender: UIButton) {
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
