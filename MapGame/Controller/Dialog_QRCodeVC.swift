//
//  Dialog_QRCodeVC.swift
//  MapGame
//
//  Created by omid on 11/29/1397 AP.
//  Copyright © 1397 omid. All rights reserved.
//

import UIKit

class Dialog_QRCodeVC: UIViewController {

    @IBOutlet weak var barcodeLbl_onButton: UILabel!
    @IBOutlet weak var panelLbl_onButton: UILabel!
    
    
    @IBOutlet weak var scanBarcodBtn: BordredButon!
    
    @IBOutlet weak var routingBtn: BordredButon!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scanBarcodBtn.addTarget(self, action: #selector(scanBarcod(_:)), for: .touchUpInside)
        scanBarcodBtn.addTarget(self, action: #selector(scanBarcod2(_:)), for: .touchDown)
        routingBtn.addTarget(self, action: #selector(scanBarcod(_:)), for: .touchUpInside)
        routingBtn.addTarget(self, action: #selector(scanBarcod2(_:)), for: .touchDown)
    }
    
    @objc func scanBarcod(_ sender:BordredButon){
        if sender == scanBarcodBtn {
            UIView.animate(withDuration: 0.3) {
                self.barcodeLbl_onButton.alpha = 1
            }
        }else if sender == routingBtn {
            UIView.animate(withDuration: 0.3) {
                self.panelLbl_onButton.alpha = 1
            }
        }
    }
    @objc func scanBarcod2(_ sender:BordredButon){
        if sender == scanBarcodBtn {
            self.barcodeLbl_onButton.alpha = 0.3
        }else if sender == routingBtn {
            self.panelLbl_onButton.alpha = 0.3
        }
    }
    
    @IBAction func closeDialogAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func scanBarcodeAction(_ sender: BordredButon) {
        
    }
    
    
    @IBAction func routingAction(_ sender: BordredButon) {
        
    }
    
}
