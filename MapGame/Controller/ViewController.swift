//
//  ViewController.swift
//  MapGame
//
//  Created by omid on 11/23/1397 AP.
//  Copyright © 1397 omid. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import GooglePlaces


class ViewController: UIViewController {
    
    @IBOutlet weak var segmentControll: UISegmentedControl!
    @IBOutlet weak var mapView: GMSMapView!
    
    @IBOutlet weak var menueBarItem: UIBarButtonItem!
    
    
    @IBOutlet weak var gradientView: UIView!
    var marker:GMSMarker = GMSMarker()
    
    var myLocation:CLLocation!
    let locationmanager:CLLocationManager = CLLocationManager()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
         prepareLocationManager()
         prepareSegmentedControllerAndItemBar()
        let camera = GMSCameraPosition.camera(withTarget: CLLocationCoordinate2D(latitude: 36.686834, longitude: 48.495697), zoom: 16.0)
        mapView.delegate = self
        //mapView.settings.myLocationButton = true
        mapView.isMyLocationEnabled = true
        mapView.animate(to: camera)
        
        
        do {
            // Set the map style by passing the URL of the local file. Make sure style.json is present in your project
            if let styleURL = Bundle.main.url(forResource: "map_style", withExtension: "json") {
                mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                print("Unable to find style.json")
            }
        } catch {
            print("The style definition could not be loaded: \(error)")
        }
    }

    
    func prepareLocationManager(){
        locationmanager.delegate = self
        locationmanager.requestAlwaysAuthorization()
        locationmanager.startUpdatingLocation()
        locationmanager.desiredAccuracy = kCLLocationAccuracyBest
    }
    func prepareSegmentedControllerAndItemBar(){
        menueBarItem.setTitleTextAttributes([NSAttributedString.Key.font :font_MapGame(size: 20)], for: .normal)
        menueBarItem.setTitleTextAttributes([NSAttributedString.Key.font :font_MapGame(size: 20)], for: .selected)
        let titleTextAttributesNormal = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1),NSAttributedString.Key.font:font_IRANSansMobileFaNum_Bold(size: 15)] as [NSAttributedString.Key : Any]
        let titleTextAttributesSelected = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.2605174184, green: 0.2605243921, blue: 0.260520637, alpha: 1),NSAttributedString.Key.font:font_IRANSansMobileFaNum_Bold(size: 15)] as [NSAttributedString.Key : Any]
        
        segmentControll.setTitleTextAttributes(titleTextAttributesNormal, for: .normal)
        segmentControll.setTitleTextAttributes(titleTextAttributesSelected, for: .selected)
        
        segmentControll.layer.cornerRadius = segmentControll.bounds.height / 2
        segmentControll.layer.masksToBounds = true
        segmentControll.layer.borderWidth = 1
        segmentControll.layer.borderColor = #colorLiteral(red: 0.9963751435, green: 0.8537960649, blue: 0, alpha: 1)
    }
    
    //Family: IRANSansMobile(FaNum) Font names: ["IRANSansMobileFaNum"]
    //Family: IRANSansMobileFaNum Font names: ["IRANSansMobileFaNum-Bold", "IRANSansMobileFaNum-Light"]
    //MapGame
    @IBAction func showMenueAction(_ sender: UIBarButtonItem) {
       self.performSegue(withIdentifier: "toQRReader", sender: self)
    }
    
   var firstCamera = false
}

extension ViewController:CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        myLocation = manager.location
        // update marker location
        marker.position = myLocation.coordinate
        marker.title = "MyPosition"
        marker.snippet = "Iran"
        marker.map = mapView
    }
    
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
    }
}


extension ViewController: GMSMapViewDelegate {
    func mapViewDidStartTileRendering(_ mapView: GMSMapView) {
        
    }
    
    func mapViewDidFinishTileRendering(_ mapView: GMSMapView) {
        
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        let tilt = mapView.camera.zoom < 15 ? 0:90
        let camera = GMSCameraPosition.camera(withTarget: mapView.camera.target,
                                              zoom: mapView.camera.zoom,
                                              bearing: mapView.camera.bearing,
                                              viewingAngle: Double(tilt))
        mapView.animate(to: camera)
    }
    
    
}

