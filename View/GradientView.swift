//
//  GradientView.swift
//  MapGame
//
//  Created by omid on 11/29/1397 AP.
//  Copyright © 1397 omid. All rights reserved.
//

import UIKit


@IBDesignable
class GradientView: UIView {

    @IBInspectable var starColor:UIColor =  UIColor.blue
    @IBInspectable var middleColor:UIColor = UIColor.green
    @IBInspectable var endColor:UIColor = UIColor.clear
    @IBInspectable var startPoint:CGPoint = CGPoint(x: 0.5, y: 1.0)
     @IBInspectable var endPoint:CGPoint = CGPoint(x: 0.5, y: 0.0)
    @IBInspectable var gradient: CAGradientLayer = CAGradientLayer()
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        
        // #colorLiteral(red: 0.1999762356, green: 0.200016588, blue: 0.1999709308, alpha: 1)
        gradient.colors = [starColor.cgColor,middleColor.cgColor,endColor.cgColor]
        
        gradient.startPoint = CGPoint(x: 0.5, y: 1.0)
        gradient.endPoint = CGPoint(x: 0.5, y: 0.0)
        gradient.frame = rect
        self.layer.addSublayer(gradient)
    }
 
}
