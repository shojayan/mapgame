//
//  TimerLabel.swift
//  MapGame
//
//  Created by omid on 12/4/1397 AP.
//  Copyright © 1397 omid. All rights reserved.
//

import UIKit

class TimerLabel: UILabel {

    var timer:Timer!
    var firstLblText:String?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        firstLblText = self.text
    }
    
    
    func upCounting(to:TimeInterval,timeInterval:TimeInterval,repeats:Bool,
                    counting:@escaping (TimeInterval)->Void,
                    complet:@escaping ()->Void){
        var toTime:TimeInterval = 0
        timer = Timer.scheduledTimer(withTimeInterval: timeInterval, repeats: repeats, block: { timer  in
            toTime += 1
            self.text = "\(Int(toTime))".convertToPersion()
            counting(toTime)
            if toTime == to {
                timer.invalidate()
                complet()
            }
        })
        timer.fire()
    }

    
    func downCounting(from:TimeInterval,timeInterval:TimeInterval,repeats:Bool,
                      counting:@escaping (TimeInterval)->Void,
                      complet:@escaping ()->Void){
        var fromTime:TimeInterval = from
        timer = Timer.scheduledTimer(withTimeInterval: timeInterval, repeats: repeats, block: { timer  in
            fromTime -= 1
            self.text = "\(Int(fromTime))".convertToPersion()
            counting(fromTime)
            if fromTime == 0 {
                timer.invalidate()
                complet()
            }
        })
        timer.fire()
    }
    
    func resetLable(){
        self.text = firstLblText
    }
}
