//
//  IndicatorQR_Reader.swift
//  MapGame
//
//  Created by omid on 11/28/1397 AP.
//  Copyright © 1397 omid. All rights reserved.
//

import UIKit



@IBDesignable
class IndicatorQR_Reader: UIView {

    let animcolor = CABasicAnimation(keyPath: "position")
    let shapLayer_IndicatorLine = CAShapeLayer()
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        let viewCenter = CGPoint(x: rect.width / 2 , y: rect.height / 2)
        let masksize = CGSize(width: rect.width - 100, height: rect.width - 100)
        let maskorigin = CGPoint(x: viewCenter.x - masksize.width / 2 ,
                                 y: viewCenter.y - masksize.height / 2)
       
        
        
        
        
        shapLayer_IndicatorLine.strokeColor = UIColor.red.cgColor
        shapLayer_IndicatorLine.fillColor = UIColor.clear.cgColor
        shapLayer_IndicatorLine.lineWidth = 1
        shapLayer_IndicatorLine.frame = rect
        
        let shapLayer_Border = CAShapeLayer()
        shapLayer_Border.strokeColor = UIColor.red.cgColor
        shapLayer_Border.fillColor = UIColor.clear.cgColor
        shapLayer_Border.lineWidth = 1
        shapLayer_Border.frame = rect
        
        
        
        
        
        animcolor.isAdditive = true
        animcolor.fromValue         = CGPoint(x: 0, y: 0)
        animcolor.toValue           = CGPoint(x: 0, y:  masksize.height )
        animcolor.duration          = 0.5;
        animcolor.repeatCount       = Float.infinity;
        animcolor.autoreverses      = true
        
        
        
        let path_lines = UIBezierPath()
        path_lines.move(to: maskorigin)
        path_lines.addLine(to: CGPoint(x: rect.width - 50, y: maskorigin.y))
        path_lines.close()
        shapLayer_IndicatorLine.path = path_lines.cgPath
        
        let path_rect = UIBezierPath(roundedRect: CGRect(origin: maskorigin, size: masksize), cornerRadius: 10)
        shapLayer_Border.path = path_rect.cgPath
        
        self.layer.addSublayer(shapLayer_Border)
        shapLayer_IndicatorLine.add(animcolor, forKey: "anim")
        self.layer.addSublayer(shapLayer_IndicatorLine)
    }
 
    func stopAnimating(){
        shapLayer_IndicatorLine.removeAnimation(forKey: "anim")
    }

    func startAnimating(){
        shapLayer_IndicatorLine.add(animcolor, forKey: "anim")
    }
}
