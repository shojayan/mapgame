//
//  BordredLable.swift
//  MapGame
//
//  Created by omid on 12/1/1397 AP.
//  Copyright © 1397 omid. All rights reserved.
//

import UIKit


@IBDesignable
class BordredLable: UILabel {

    @IBInspectable var borderColor:UIColor = UIColor.red
    @IBInspectable var borderWidth:CGFloat = 1
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        layer.cornerRadius = rect.height / 2
        layer.masksToBounds = true
        layer.borderColor = borderColor.cgColor
        layer.borderWidth = borderWidth
    }


}
