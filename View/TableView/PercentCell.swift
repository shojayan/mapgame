//
//  PercentCell.swift
//  MapGame
//
//  Created by omid shojaeian on 12/17/1397 AP.
//  Copyright © 1397 omid. All rights reserved.
//

import UIKit

class PercentCell: UITableViewCell {

    
    @IBOutlet weak var verticalLineView: VerticalLineDivided!
    @IBOutlet weak var percentLbl: UILabel!
    @IBOutlet weak var offTitle: UILabel!
    @IBOutlet weak var btnCoponNumberCopy: UIButton!
    @IBOutlet weak var descriptionTxt: UILabel!
    
    
    
    var delegate:PercentCellDelegate!
    var indexPath:IndexPath!
    
    override func awakeFromNib() {
        percentLbl.layer.cornerRadius = percentLbl.frame.height / 2
        percentLbl.layer.masksToBounds = true
        percentLbl.layer.borderColor = #colorLiteral(red: 1, green: 0.8198096156, blue: 0, alpha: 1)
        percentLbl.layer.borderWidth = 2
    }
    
    func filledPercentLbl(isFill:Bool){
        if isFill{
            percentLbl.backgroundColor = #colorLiteral(red: 1, green: 0.8198096156, blue: 0, alpha: 1)
            percentLbl.textColor = #colorLiteral(red: 0.1999762356, green: 0.200016588, blue: 0.1999709308, alpha: 1)
        }else {
            percentLbl.backgroundColor = #colorLiteral(red: 0.1999762356, green: 0.200016588, blue: 0.1999709308, alpha: 1)
            percentLbl.textColor = #colorLiteral(red: 1, green: 0.8198096156, blue: 0, alpha: 1)
        }
    }
    @IBAction func copyCoponAction(_ sender: UIButton) {
        delegate.copyCopon(indexPath: indexPath)
    }
}
protocol PercentCellDelegate:class {
    func copyCopon(indexPath:IndexPath)
}
