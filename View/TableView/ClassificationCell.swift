//
//  ClassificationCell.swift
//  MapGame
//
//  Created by omid shojaeian on 12/22/1397 AP.
//  Copyright © 1397 omid. All rights reserved.
//

import UIKit

class ClassificationCell: UITableViewCell {

    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var rowNumber: UILabel!
    @IBOutlet weak var totalPointsLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
