//
//  AnswerCell.swift
//  MapGame
//
//  Created by omid on 11/30/1397 AP.
//  Copyright © 1397 omid. All rights reserved.
//

import UIKit

class AnswerCell: UITableViewCell {

    @IBOutlet weak var tikTxt: UILabel!
    @IBOutlet weak var  button:BordredButon!
    
    var indexOfCell:IndexPath!
    
    let tik = ""
    let untik = ""
    
    var delegate:AnswerCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        NotificationCenter.default.addObserver(self, selector: #selector(unckeckIt(_:)), name: NSNotification.Name(rawValue: "answered"), object: nil)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func changeButtonTitle(to:String){
        button.setTitle(to, for: .normal)
        button.setTitle(to, for: .selected)
    }
    @IBAction func answerAction(_ sender: BordredButon) {
        postChecked()
        delegate?.selectedAnswer(indexPath: indexOfCell)
    }
    
    func postChecked(){
        NotificationCenter.default.post(name: NSNotification.Name("answered"), object: indexOfCell)
    }
    
   @objc func unckeckIt(_ sender:Notification){
    if (sender.object as! IndexPath) == indexOfCell {
        button.backgroundColor = #colorLiteral(red: 0.2605174184, green: 0.2605243921, blue: 0.260520637, alpha: 1)
        checked()
        }else {
            button.backgroundColor = #colorLiteral(red: 0.3234693706, green: 0.3234777451, blue: 0.3234732151, alpha: 1)
            unchecked()
            }
        }   
    
    func checked(){
        tikTxt.text = ""
    }
    
    func unchecked(){
        tikTxt.text = ""
    }
}
protocol AnswerCellDelegate:class {
    func selectedAnswer(indexPath:IndexPath)
}
