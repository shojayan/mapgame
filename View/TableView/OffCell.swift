//
//  OffCell.swift
//  MapGame
//
//  Created by omid shojaeian on 12/15/1397 AP.
//  Copyright © 1397 omid. All rights reserved.
//

import UIKit

class OffCell: UITableViewCell,UITableViewDataSource,UITableViewDelegate,PercentCellDelegate {
    
    @IBOutlet weak var imageShope: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var descriptionText: UITextView!
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var heightTableView: NSLayoutConstraint!
    
    
    @IBOutlet weak var infoView: InfoView_index!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.layer.cornerRadius = 40
        containerView.layer.masksToBounds = true
        containerView.layer.borderColor = #colorLiteral(red: 1, green: 0.8233510256, blue: 0, alpha: 1)
        containerView.layer.borderWidth = 2
        
        //tableview.dataSource = self
        //tableview.delegate = self
        
        
    }
    
    override func didMoveToSuperview() {
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func copyAction(_ sender: Any) {
        print("copy action done")
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        descriptionText.text = "\(infoView.indexPath)"
        return infoView.allData
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PercentCell") as! PercentCell
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func copyCopon(indexPath: IndexPath) {
        
    }
    
    
}
