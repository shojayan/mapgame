//
//  HorizontalLineDivided.swift
//  MapGame
//
//  Created by omid shojaeian on 12/16/1397 AP.
//  Copyright © 1397 omid. All rights reserved.
//

import UIKit
@IBDesignable
class VerticalLineDivided: UIView {

    @IBInspectable var color:UIColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
    @IBInspectable var lineWidth:CGFloat = 2.0
    @IBInspectable var isTop:Bool = true
    @IBInspectable var isDown:Bool = true
    
    
    
    override func draw(_ rect: CGRect) {
        let shape = CAShapeLayer()
        shape.frame = rect
        shape.strokeColor = color.cgColor
        shape.fillColor = color.cgColor
        shape.lineWidth = lineWidth
        
        
        let topHLinePath = UIBezierPath()
        topHLinePath.move(to: CGPoint(x: rect.width/2, y: 0))
        topHLinePath.addLine(to: CGPoint(x: rect.width/2, y: rect.height / 2 ))
        
        let downHLinePath = UIBezierPath()
        downHLinePath.move(to: CGPoint(x: rect.width/2, y: rect.height / 2))
        downHLinePath.addLine(to: CGPoint(x: rect.width/2, y: rect.height ))
        
        let sourcePath = UIBezierPath()
        
        if isTop {
            sourcePath.append(topHLinePath)
        }
        if isDown {
            sourcePath.append(downHLinePath)
        }
        
        shape.path = sourcePath.cgPath
        layer.addSublayer(shape)
    }

}
