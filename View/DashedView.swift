//
//  DashedView.swift
//  MapGame
//
//  Created by omid shojaeian on 12/15/1397 AP.
//  Copyright © 1397 omid. All rights reserved.
//

import UIKit

@IBDesignable
class DashedView: UIView {

    @IBInspectable var color:UIColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
    @IBInspectable var lineWidth:CGFloat = 2.0
    @IBInspectable var dashWidth:NSNumber = 7.0
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        let shape = CAShapeLayer()
        shape.frame = rect
        shape.strokeColor = color.cgColor
        shape.fillColor = color.cgColor
        shape.lineWidth = lineWidth
        shape.lineDashPattern = [dashWidth, dashWidth]
        
        let lineDashedPath = UIBezierPath()
        lineDashedPath.move(to: CGPoint(x: 0, y: rect.height / 2 ))
        lineDashedPath.addLine(to: CGPoint(x: rect.width, y: rect.height / 2 ))
        
        shape.path = lineDashedPath.cgPath
        
        layer.addSublayer(shape)
    }
 

}
