//
//  ShapeTitle2.swift
//  MapGame
//
//  Created by omid on 11/28/1397 AP.
//  Copyright © 1397 omid. All rights reserved.
//

import UIKit


@IBDesignable
class ShapeTitle_mycustom: UIView {

    var centerWidth:CGFloat = 187.5
    @IBInspectable var height:CGFloat = 90
    @IBInspectable var xAccruncy:CGFloat = 0
    @IBInspectable var yAccruncy:CGFloat = 0
    @IBInspectable var radiuse:CGFloat = 90
    @IBInspectable var isShowCircle:Bool = true
    @IBInspectable var lineWidth:CGFloat = 2
    @IBInspectable var strokeColor:UIColor = UIColor.red
    @IBInspectable var fillColor:UIColor = UIColor.white
    @IBInspectable var controll_Up:CGFloat = 35
    @IBInspectable var controll_down:CGFloat = 30
    @IBInspectable var extraVal:CGFloat = 0
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
        let shapeLayer = CAShapeLayer()
        
        shapeLayer.strokeColor = strokeColor.cgColor
        shapeLayer.fillColor = fillColor.cgColor
        shapeLayer.lineWidth = lineWidth
        shapeLayer.strokeStart = 0
        shapeLayer.strokeEnd = 1
        shapeLayer.frame = rect
        
        
        let path = UIBezierPath()
        centerWidth = rect.width / 2
        path.move(to: CGPoint(x: 0 - extraVal , y: rect.height))
        path.addLine(to: CGPoint(x: centerWidth - (2 * height), y: rect.height))
        path.addCurve(to: CGPoint(x: centerWidth, y: rect.height - height),
                      controlPoint1: CGPoint(x: centerWidth - controll_down , y: rect.height),
                      controlPoint2: CGPoint(x: centerWidth - controll_Up, y: rect.height - height))
        path.addCurve(to: CGPoint(x: centerWidth + (2 * height), y: rect.height),
                      controlPoint1: CGPoint(x: centerWidth + controll_Up , y: rect.height - height),
                      controlPoint2: CGPoint(x: centerWidth + controll_down, y: rect.height))
        path.addLine(to: CGPoint(x: rect.width + extraVal, y: rect.height))
        path.addLine(to: CGPoint(x: rect.width + extraVal, y: 0 ))
        path.addLine(to: CGPoint(x: 0 - extraVal , y: 0 ))
        path.addLine(to: CGPoint(x: 0 - extraVal, y: rect.height))
        let pathCircle = UIBezierPath()
        if isShowCircle {
            pathCircle.addArc(withCenter: CGPoint(x: centerWidth + xAccruncy,
                                                  y: rect.height + yAccruncy),
                              radius: radiuse ,
                              startAngle: 0,
                              endAngle: CGFloat.pi * 2 ,
                              clockwise: true)
            path.append(pathCircle)
        }
        path.close()
        
        shapeLayer.path = path.cgPath
        self.layer.addSublayer(shapeLayer)
        
        
        let shapeLayerMask = CAShapeLayer()
        shapeLayerMask.strokeColor = fillColor.cgColor
        shapeLayerMask.fillColor = fillColor.cgColor
        shapeLayerMask.lineWidth = lineWidth
        shapeLayerMask.strokeStart = 0
        shapeLayerMask.strokeEnd = 1
        shapeLayerMask.frame = CGRect(x: 0, y: 0, width: rect.width, height: lineWidth * 2 )
        let maskPath = UIBezierPath(rect: CGRect(x: 0, y: 0, width: rect.width, height: lineWidth * 2 ))
        shapeLayerMask.path = maskPath.cgPath
        self.layer.addSublayer(shapeLayerMask)
        
        
        
        let shapeLayerMaskLeft = CAShapeLayer()
        shapeLayerMaskLeft.strokeColor = fillColor.cgColor
        shapeLayerMaskLeft.fillColor = fillColor.cgColor
        shapeLayerMaskLeft.lineWidth = lineWidth
        shapeLayerMaskLeft.strokeStart = 0
        shapeLayerMaskLeft.strokeEnd = 1
        shapeLayerMaskLeft.frame = CGRect(x: 0, y: 0, width: lineWidth, height: rect.height - lineWidth)
        let maskPathLeft = UIBezierPath(rect: CGRect(x: 0, y: 0, width: lineWidth, height: rect.height - lineWidth))
        shapeLayerMaskLeft.path = maskPathLeft.cgPath
        self.layer.addSublayer(shapeLayerMaskLeft)
        
        
        
        let shapeLayerMaskRight = CAShapeLayer()
        shapeLayerMaskRight.strokeColor = fillColor.cgColor
        shapeLayerMaskRight.fillColor = fillColor.cgColor
        shapeLayerMaskRight.lineWidth = lineWidth
        shapeLayerMaskRight.strokeStart = 0
        shapeLayerMaskRight.strokeEnd = 1
        shapeLayerMaskRight.frame = CGRect(x: rect.width - lineWidth, y: 0, width: lineWidth, height: rect.height - lineWidth)
        let maskPathRight = UIBezierPath(rect: CGRect(x: 0, y: 0, width: lineWidth, height: rect.height - lineWidth))
        shapeLayerMaskRight.path = maskPathRight.cgPath
        self.layer.addSublayer(shapeLayerMaskRight)
    }
    
    
    
   
}

