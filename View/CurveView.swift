//
//  CurveView.swift
//  MapGame
//
//  Created by omid shojaeian on 12/22/1397 AP.
//  Copyright © 1397 omid. All rights reserved.
//

import UIKit

@IBDesignable
class CurveView: UIView {

    @IBInspectable var colorFill:UIColor = #colorLiteral(red: 1, green: 0.8198096156, blue: 0, alpha: 1)
    @IBInspectable var colorStrok:UIColor = #colorLiteral(red: 1, green: 0.8198096156, blue: 0, alpha: 1)
    @IBInspectable var lineWidth:CGFloat = 2.0
    @IBInspectable var startX:CGFloat = 0.0
    @IBInspectable var endX:CGFloat = 10.0
    @IBInspectable var distance:CGFloat = 20.0
    @IBInspectable var acrunccy:CGFloat =  0 
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = colorStrok.cgColor
        shapeLayer.fillColor = colorFill.cgColor
        shapeLayer.lineWidth = lineWidth
        
        
        let pathCurv = UIBezierPath()
        pathCurv.move(to: CGPoint(x: startX, y: 0))
        
        pathCurv.addCurve(to: CGPoint(x: endX, y: rect.height),
                          controlPoint1: CGPoint(x: distance, y: 0),
                          controlPoint2: CGPoint(x: endX - distance + acrunccy , y: rect.height ))
        
        pathCurv.addLine(to: CGPoint(x: rect.width , y: rect.height ))
        pathCurv.addLine(to: CGPoint(x: rect.width , y: 0 ))
        pathCurv.addLine(to: CGPoint(x: startX , y: 0 ))
        
        pathCurv.close()
        
        shapeLayer.path = pathCurv.cgPath
        
        layer.addSublayer(shapeLayer)
        
    }
 

}
