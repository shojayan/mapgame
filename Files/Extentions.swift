//
//  Extentions.swift
//  MediaRasa2
//
//  Created by omid on 7/24/1397 AP.
//  Copyright © 1397 omid. All rights reserved.
//

import Foundation
import UIKit
import NVActivityIndicatorView
import CoreLocation
import WazeKit

extension UIViewController {
//    func changeBackButtonwith(title:String,imageIcone:UIImage?){
//        self.navigationController?.navigationBar.backIndicatorImage = imageIcone != nil ? imageIcone:UIImage()
//        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = imageIcone != nil ? imageIcone:UIImage()
//        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: title, style: UIBarButtonItem.Style.done, target: nil, action: nil)
//    }
    func registerUser(info:RegisterFcmToken.UserTokenViewModel,userImage:UIImage){
        let encoder = JSONEncoder()
        UserDefaults.standard.set(try! encoder.encode(info), forKey: "userRegistered")
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let fileName = "image001.png" // name of the image to be saved
        let fileURL = documentsDirectory.appendingPathComponent(fileName)
        //if let data = UIImageJPEGRepresentation(userImage, 1.0),!FileManager.default.fileExists(atPath:fileURL.path){
        if  let data = userImage.jpegData(compressionQuality: 1.0) { //}, !FileManager.default.fileExists(atPath: fileURL.path){
            do {
                try data.write(to: fileURL)
                print("file saved")
            } catch {
                print("error saving file:", error)
            }
        }
    }
    func readRegisteredUserInfo()->RegisterFcmToken.UserTokenViewModel? {
        if  UserDefaults.standard.value(forKey: "userRegistered") == nil {
            return nil
        }else {
            let decoder = JSONDecoder()
            return try! decoder.decode(RegisterFcmToken.UserTokenViewModel.self, from: (UserDefaults.standard.value(forKey: "userRegistered") as! Data))
        }
    }
    func readUserImage()->UIImage {
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        let paths = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        if let dirPath = paths.first{
            let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent("image001.png")
            let image    = UIImage(contentsOfFile: imageURL.path)
            return image!
        }
         return UIImage.init(named: "default.png")!
    }
    func deviceID()->String? {
        return UIDevice.current.identifierForVendor?.uuidString
    }
    func showCustomDialog(message:String? = "دوباره تلاش کنید",btnTitle:String? = "بازگشت",completionSHow: (() -> Void)? = nil,completionDismiss: (() -> Void)? = nil){
        let dialogAlert = self.storyboard?.instantiateViewController(withIdentifier: "Dialog_AlertVC") as! Dialog_AlertVC
        dialogAlert.textAlertRecieved = message!
        dialogAlert.btnTitle = btnTitle!
        dialogAlert.actions[.button] = {
            dialogAlert.dismiss(animated: true, completion: completionDismiss)
        }
        DispatchQueue.main.async {
         self.present(dialogAlert, animated: true, completion: completionSHow)
        }
    }
    func showCustomDialogWithAction(message:String? = "دوباره تلاش کنید",btnTitle:String? = "تلاش دوباره",completionSHow: (() -> Void)? = nil,completionDismiss: (() -> Void)? = nil,action:@escaping ()->Void){
        let dialogAlert = self.storyboard?.instantiateViewController(withIdentifier: "Dialog_AlertVC") as! Dialog_AlertVC
        dialogAlert.textAlertRecieved = message!
        dialogAlert.btnTitle = btnTitle!
        dialogAlert.actions[.button] = {
            action()
        }
        DispatchQueue.main.async {
            self.present(dialogAlert, animated: true, completion: completionSHow)
        }
    }
    func imageWithImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    func openWaze(location : CLLocationCoordinate2D,notInstalled:@escaping ()->Void) {
        let wazeLauncher: WazeLauncher = WazeLauncher()
        
        if UIApplication.shared.canOpenURL(URL(string: "waze://")!) {
            let _ = wazeLauncher.navigate(toCoordinate: location)
        }
        else {
            notInstalled()
        }
    }
    
    func openInstagram(id : String,notInstalled:@escaping ()->Void){
        if UIApplication.shared.canOpenURL(URL(string: "instagram://")!){
            let instagramHooks = "instagram://user?username=\(id)"
            let instagramUrl = URL(string: instagramHooks)
            UIApplication.shared.open(instagramUrl!, options: [:], completionHandler: nil)
        }
        else {
            notInstalled()
        }
    }
        func openInstagramHashtag(hashtag: String,notInstalled:@escaping ()->Void){
            if UIApplication.shared.canOpenURL(URL(string: "instagram://")!){
                let instagramHooks = "instagram://tag?name=\(hashtag)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                let instagramUrl = URL(string: instagramHooks!)
                UIApplication.shared.open(instagramUrl!, options: [:], completionHandler: nil)
            }
            else {
                notInstalled()
            }
            
            /*
             if(searchQuery.contains("@")){
             uri=Uri.parse("http://instagram.com/"+searchQuery.replace("@","")+"/");
             }else{
             uri=Uri.parse("http://instagram.com/explore/tags/"+searchQuery.replace("#","")+"/");
             }
             */
    }
    
    func openGoogleMap(location : CLLocationCoordinate2D,notInstalled:@escaping ()->Void){
        if UIApplication.shared.canOpenURL(URL(string: "comgooglemaps://")!){
            if let url = URL(string: "comgooglemaps://?saddr=&daddr=\(location.latitude),\(location.longitude)&directionsmode=driving") {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }else {
            notInstalled()
        }
        
    }
    func font_IRANSansMobileFaNum(size:CGFloat)->UIFont {
        return UIFont(name: "IRANSansMobileFaNum", size: size)!
    }
    func font_IRANSansMobileFaNum_Bold(size:CGFloat)->UIFont {
        return UIFont(name: "IRANSansMobileFaNum-Bold", size: size)!
    }
    func font_IRANSansMobileFaNum_Light(size:CGFloat)->UIFont{
        return UIFont(name: "IRANSansMobileFaNum-Light", size: size)!
    }
    func font_MapGame(size:CGFloat)->UIFont {
        return UIFont(name: "MapGame", size: size)!
    }
    func convertStringToURL(str:String)->URL?{
        let str2 = str.replacingOccurrences(of: "ك", with: "ک").replacingOccurrences(of: "ي", with: "ی").addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        return URL(string: str2!)
    }
    
    
    /// created from Library with pod 'NVActivityIndicatorView'
    func loadingView(isShow:Bool,type:NVActivityIndicatorType, font:UIFont?,loadText:String,textColor:UIColor,backColor:UIColor,shapeColor:UIColor){
        if isShow {
            if self.view.viewWithTag(150) == nil {
                let loadingView = UIView(frame: self.view.frame)
                let loadingText:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: 70, height: 70))
                let circleIndicator:NVActivityIndicatorView = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50), type: type, color: shapeColor, padding: 10)
                
                loadingView.tag = 150
                loadingText.tag = 151
                circleIndicator.tag = 152
                
                loadingView.backgroundColor = backColor
                
                loadingText.textColor = textColor
                loadingText.text = loadText
                if font != nil { loadingText.font = font! }
                loadingText.sizeToFit()
                
                loadingView.center = self.view.center
                circleIndicator.center = self.view.convert(self.view.center, to: loadingView)
                loadingText.center = CGPoint(x: self.view.convert(self.view.center, to: loadingView).x , y: self.view.convert(self.view.center, to: loadingView).y + 50)
                
                loadingView.addSubview(circleIndicator)
                loadingView.addSubview(loadingText)
                circleIndicator.startAnimating()
                self.view.addSubview(loadingView)
                
            }else {
                
            }
        }else{
            if self.view.viewWithTag(150) != nil {
                UIView.animate(withDuration: 0.5, delay: 0.0, options: [], animations: {
                    self.view.viewWithTag(150)?.alpha = 0
                }) { _ in
                    self.view.viewWithTag(150)?.viewWithTag(151)?.removeFromSuperview()
                    self.view.viewWithTag(150)?.viewWithTag(152)?.removeFromSuperview()
                    self.view.viewWithTag(150)?.removeFromSuperview()
                }
            }
        }
    }
    func loadingViewForThisProject(isShow:Bool){
        self.loadingView(isShow: isShow, type: NVActivityIndicatorType.circleStrokeSpin, font: font_IRANSansMobileFaNum(size: 10), loadText: "چند لحظه صبر کنید", textColor: #colorLiteral(red: 1, green: 0.8233510256, blue: 0, alpha: 1), backColor: #colorLiteral(red: 0.2654630829, green: 0.2654630829, blue: 0.2654630829, alpha: 0.60546875), shapeColor: #colorLiteral(red: 1, green: 0.8233510256, blue: 0, alpha: 1))
    }
    func createGap(withDuration:TimeInterval,
                   startGap:@escaping (UIViewController)->Void,
                   endGap:@escaping (UIViewController)->Void){
        startGap(self)
        var seconds:TimeInterval = 0
        Timer.scheduledTimer(withTimeInterval: 1 , repeats: true) { timer  in
            seconds += 1
            if seconds == withDuration {
                print(seconds)
                timer.invalidate()
                endGap(self)
            }
        }
    }
    func createCancellableGeneralAlert(title:String,message:String,cancelTitle:String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let alertCancelAction = UIAlertAction(title: cancelTitle, style: .cancel, handler: nil)
        alert.addAction(alertCancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    func createCancellableGeneralActionAlert(title:String,message:String,cancelTitle:String,action:@escaping (UIAlertController)->Void){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let alertCancelAction = UIAlertAction(title: cancelTitle, style: .cancel) { alertAction in
            action(alert)
        }
        alert.addAction(alertCancelAction)
        self.present(alert, animated: true, completion: nil)
    }
}


extension String {
    func components(withLength length: Int) -> [String] {
        return stride(from: 0, to: self.count, by: length).map {
            let start = self.index(self.startIndex, offsetBy: $0)
            let end = self.index(start, offsetBy: length, limitedBy: self.endIndex) ?? self.endIndex
            return String(self[start..<end])
        }
    }
    func stringToUTF16String (str:String) -> String {
        
        var retval:String = str
        retval = retval.replacingOccurrences(of: "0", with: "۰", options: .literal, range: nil)
        retval = retval.replacingOccurrences(of: "1", with: "۱", options: .literal, range: nil)
        retval = retval.replacingOccurrences(of: "2", with: "۲", options: .literal, range: nil)
        retval = retval.replacingOccurrences(of: "3", with: "۳", options: .literal, range: nil)
        retval = retval.replacingOccurrences(of: "4", with: "۴", options: .literal, range: nil)
        retval = retval.replacingOccurrences(of: "5", with: "۵", options: .literal, range: nil)
        retval = retval.replacingOccurrences(of: "6", with: "۶", options: .literal, range: nil)
        retval = retval.replacingOccurrences(of: "7", with: "۷", options: .literal, range: nil)
        retval = retval.replacingOccurrences(of: "8", with: "۸", options: .literal, range: nil)
        retval = retval.replacingOccurrences(of: "9", with: "۹", options: .literal, range: nil)
        return retval
    }
    func convertPersionToEngilish(str:String)->String{
        var retval:String = str
        retval = retval.replacingOccurrences(of: "۰", with: "0", options: .literal, range: nil)
        retval = retval.replacingOccurrences(of: "۱", with: "1", options: .literal, range: nil)
        retval = retval.replacingOccurrences(of: "۲", with: "2", options: .literal, range: nil)
        retval = retval.replacingOccurrences(of: "۳", with: "3", options: .literal, range: nil)
        retval = retval.replacingOccurrences(of: "۴", with: "4", options: .literal, range: nil)
        retval = retval.replacingOccurrences(of: "۵", with: "5", options: .literal, range: nil)
        retval = retval.replacingOccurrences(of: "۶", with: "6", options: .literal, range: nil)
        retval = retval.replacingOccurrences(of: "۷", with: "7", options: .literal, range: nil)
        retval = retval.replacingOccurrences(of: "۸", with: "8", options: .literal, range: nil)
        retval = retval.replacingOccurrences(of: "۹", with: "9", options: .literal, range: nil)
        return retval
    }
    func convertToPersion()->String{
        return stringToUTF16String(str: self)
    }
}
extension UIApplication {
    
    class var topViewController: UIViewController? {
        return getTopViewController()
    }
    
    private class func getTopViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return getTopViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return getTopViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return getTopViewController(base: presented)
        }
        return base
    }
}

extension Equatable {
    func share() {
        let activity = UIActivityViewController(activityItems: [self], applicationActivities: nil)
        UIApplication.topViewController?.present(activity, animated: true, completion: nil)
    }
}


extension UIDevice {
    
    static let modelName: String = {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        func mapToDevice(identifier: String) -> String { // swiftlint:disable:this cyclomatic_complexity
            #if os(iOS)
            switch identifier {
            case "iPod5,1":                                 return "iPod Touch 5"
            case "iPod7,1":                                 return "iPod Touch 6"
            case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
            case "iPhone4,1":                               return "iPhone 4s"
            case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
            case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
            case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
            case "iPhone7,2":                               return "iPhone 6"
            case "iPhone7,1":                               return "iPhone 6 Plus"
            case "iPhone8,1":                               return "iPhone 6s"
            case "iPhone8,2":                               return "iPhone 6s Plus"
            case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
            case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
            case "iPhone8,4":                               return "iPhone SE"
            case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
            case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
            case "iPhone10,3", "iPhone10,6":                return "iPhone X"
            case "iPhone11,2":                              return "iPhone XS"
            case "iPhone11,4", "iPhone11,6":                return "iPhone XS Max"
            case "iPhone11,8":                              return "iPhone XR"
            case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
            case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
            case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
            case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
            case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
            case "iPad6,11", "iPad6,12":                    return "iPad 5"
            case "iPad7,5", "iPad7,6":                      return "iPad 6"
            case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
            case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
            case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
            case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
            case "iPad6,3", "iPad6,4":                      return "iPad Pro (9.7-inch)"
            case "iPad6,7", "iPad6,8":                      return "iPad Pro (12.9-inch)"
            case "iPad7,1", "iPad7,2":                      return "iPad Pro (12.9-inch) (2nd generation)"
            case "iPad7,3", "iPad7,4":                      return "iPad Pro (10.5-inch)"
            case "iPad8,1", "iPad8,2", "iPad8,3", "iPad8,4":return "iPad Pro (11-inch)"
            case "iPad8,5", "iPad8,6", "iPad8,7", "iPad8,8":return "iPad Pro (12.9-inch) (3rd generation)"
            case "AppleTV5,3":                              return "Apple TV"
            case "AppleTV6,2":                              return "Apple TV 4K"
            case "AudioAccessory1,1":                       return "HomePod"
            case "i386", "x86_64":                          return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "iOS"))"
            default:                                        return identifier
            }
            #elseif os(tvOS)
            switch identifier {
            case "AppleTV5,3": return "Apple TV 4"
            case "AppleTV6,2": return "Apple TV 4K"
            case "i386", "x86_64": return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "tvOS"))"
            default: return identifier
            }
            #endif
        }
        
        return mapToDevice(identifier: identifier)
    }()
    
}

extension UIImage {
    func toBase64() -> String? {
        guard let imageData = self.pngData() else { return nil }
        return imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
}


extension UITableView {
    func sizeHeaderToFit() {
        if let headerView = self.tableHeaderView {
            
            headerView.setNeedsLayout()
            headerView.layoutIfNeeded()
            
            let height = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
            var frame = headerView.frame
            frame.size.height = height
            headerView.frame = frame
            
            self.tableHeaderView = headerView
        }
    }
    
    func sizeFooterToFit() {
        if let footerView = self.tableFooterView {
            footerView.setNeedsLayout()
            footerView.layoutIfNeeded()
            
            let height = footerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
            var frame = footerView.frame
            frame.size.height = height
            footerView.frame = frame
            
            self.tableFooterView = footerView
        }
    }
}
