//
//  GamePlaySingleton.swift
//  MapGame
//
//  Created by omid shojaeian on 12/9/1397 AP.
//  Copyright © 1397 omid. All rights reserved.
//

import Foundation
import GoogleMaps


class GamePlaySingleton {
    
    var levelesModel:GamePlay.GamePlayLeveleModel?
    var markerDataIndexFromMapVC:Int?
    
    private init() { }
    static let main = GamePlaySingleton()
    
    func updateSingleTone(levelesModel:GamePlay.GamePlayLeveleModel){
        self.levelesModel = levelesModel
    }
    func markerDataIndexFromMapVCUpdate(index:Int){
        markerDataIndexFromMapVC = index
    }
    func getMarkerInfo()->GamePlay.GamePlayLevelesModel_extra_item?{
        if levelesModel != nil , markerDataIndexFromMapVC != nil {
            return levelesModel!.extra![markerDataIndexFromMapVC!]
        }else{
            return nil
        }
    }
    func updateMarkerQuestionData(question:GetQuestion.QuestionModel){
        if levelesModel != nil , markerDataIndexFromMapVC != nil {
            var  answers:[GamePlay.GamePlayLevelesModel_extra_item_barcode_question_answers]  = [GamePlay.GamePlayLevelesModel_extra_item_barcode_question_answers]()
            for answer in question.extra!.answers!{
                answers.append(GamePlay.GamePlayLevelesModel_extra_item_barcode_question_answers.init(id: answer.id,
                                                                                                      body: answer.body,
                                                                                                      imageUrl: answer.imageUrl,
                                                                                                      isAnswer: answer.isAnswer))
            }
            
levelesModel!.extra![markerDataIndexFromMapVC!].barcode!.question = GamePlay.GamePlayLevelesModel_extra_item_barcode_question.init(id: question.extra!.id,body: question.extra!.body,imageUrl: question.extra!.imageUrl,minTime: question.extra!.minTime,maxTime: question.extra!.maxTime,alfa: question.extra!.alfa,answerStatus: 0 ,answers:answers )
            
            changemarker(toImage: #imageLiteral(resourceName: "img_pin_blue"), statuse: 0)
           
        }else{
            
        }
    }
    
    func updateMarketData(data:SendAnswer.AnswerModel){
        levelesModel!.extra![markerDataIndexFromMapVC!].starRate = data.extra!.level!.starRate ?? 0
        levelesModel!.extra![markerDataIndexFromMapVC!].bouncePoint = data.extra!.level!.bouncePoint ?? 0
    }
    
    func changemarker(toImage:UIImage,statuse:Int){
        if levelesModel!.extra![markerDataIndexFromMapVC!].barcode!.question != nil {
            levelesModel!.extra![markerDataIndexFromMapVC!].barcode!.question!.answerStatus = statuse
        }
        MapVCSingleton.main.mapVC?.allMarkers.filter({  marker in
            if (marker.userData as! Int) == self.markerDataIndexFromMapVC! {
                return true
            }else{
                return false
            }
        }).first?.icon = imageWithImage(image: toImage, scaledToSize: CGSize(width: 50, height: 50))
    }
    func imageWithImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
}
